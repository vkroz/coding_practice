package algo;

import org.junit.Test;

import static java.lang.String.format;
import static org.junit.Assert.assertArrayEquals;

/**
 * stock_prices_yesterday = [10, 7, 5, 8, 11, 9]
 * <p>
 * get_max_profit(stock_prices_yesterday)
 * #  returns 6 (buying for $5 and selling for $11)
 */
public class MaxArraySlice {
    /**
     * Returns array of 3 elements: max slice value, left index, right index
     */
    private int[] solution(int[] arr) {
        int[] retval = new int[3];

        int ixR = 0;
        int ixL = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[ixR + 1] > arr[ixR] && ixR < arr.length - 1) {
                ixR = ixR + 1;
            } else if (arr[ixL + 1] < arr[ixL] && ixR < arr.length - 1) {
                ixL = ixL + 1;
                if (ixR < ixL) {
                    ixR = ixL;
                }
            }

        }

        retval[0] = arr[ixR] - arr[ixL];
        retval[1] = ixL;
        retval[2] = ixR;
        return retval;
    }

    @Test public void test1() {
        int[] stock_prices_yesterday = {10, 7, 5, 8, 11, 9};
        int[] ret = (new MaxArraySlice()).solution(stock_prices_yesterday);

        assertArrayEquals(new int[] {6, 2, 4}, ret);

    }

    @Test public void test2() {
        int[] stock_prices_yesterday = {10, 9, 8, 7, 6, 5};
        int[] ret = (new MaxArraySlice()).solution(stock_prices_yesterday);

        assertArrayEquals(new int[] {6, 2, 4}, ret);

    }
}
