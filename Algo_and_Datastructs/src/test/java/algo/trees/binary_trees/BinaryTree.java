package algo.trees.binary_trees;


import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BinaryTree {

  public static class Node {

    int data;
    Node left;
    Node right;

    public Node(int data) {
      this.data = data;
      this.left = null;
      this.right = null;
    }

    public Node() {
    }

    public void setChilds(Node left, Node right) {
      this.left = left;
      this.right = right;
    }

    public void setData(int data) {
      this.data = data;
    }
  }

  private Node root;

  public BinaryTree() {
    root = new Node();
  }

  public Node getRoot() {
    return root;
  }

  public int size() {
    return this.size(root);
  }

  private int size(Node node) {
    if (node == null) return (0);
    else {
      return (size(node.left) + 1 + size(node.right));
    }
  }

  /**
   * Compute tree size using DFS traversal
   *
   * @return tree size
   */
  public int sizeDfs() {
    Stack<Node> stack = new Stack<Node>();
    stack.push(root);
    int size = 0;
    while (stack.size() > 0) {
      Node n = stack.pop();
      System.out.printf("%d ", n.data);
      size++;
      if (null != n.right)
        stack.push(n.right);
      if (null != n.left)
        stack.push(n.left);
    }
    System.out.println("");
    return size;
  }

  /**
   * Compute tree size using BFS traversal
   *
   * @return tree size
   */
  public int sizeBfs() {
    Queue<Node> queue = new LinkedList<Node>();
    queue.add(root);
    int size = 0;
    while (queue.size() > 0) {
      Node n = queue.remove();
      System.out.printf("%d ", n.data);
      size++;
      if (null != n.left)
        queue.add(n.left);
      if (null != n.right)
        queue.add(n.right);
    }
    System.out.println("");
    return size;
  }

  public int height() {
    return this.height(root);
  }

  private int height(Node n) {
    if (null == n)
      return 0;

    int height = Math.max(this.height(n.left) + 1, this.height(n.right) + 1);
    return height;
  }

  /**
   * 1 2 3 4 5 6 7 8 9
   * <p>
   * 1
   * v
   * 2 3
   * v
   * 4 5 6 7
   * v
   * 8 9 10 11 12 13
   * <p>
   * 14 15 16 17 18 19 20 21 22 23
   */
  public int heightBfs() {
    Deque<Node> queue = new LinkedList<>();
    int height = 0;
    queue.addLast(root);
    Node marker = root;
    while (queue.size() > 0) {
      Node n = queue.removeFirst();
      if (null != n.left)
        queue.addLast(n.left);
      if (null != n.right)
        queue.addLast(n.right);
      if (n == marker) {
        height++;
        marker = queue.peekLast();
      }
    }
    return height;
  }


    /**
     * Recursive lookup  -- given a node, recur down searching for the given data.
     */

  public boolean lookup(Node node, int data) {
    return true;
  }

}


