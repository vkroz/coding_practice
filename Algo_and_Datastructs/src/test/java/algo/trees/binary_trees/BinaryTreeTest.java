package algo.trees.binary_trees;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 */
public class BinaryTreeTest {

  /*
        Testing tree

             1
            / \
           /   \
          /     \
         2       3
        / \     /
       4   5   6
      /       / \
     7       8   9
 */

  BinaryTree tree;


  @Before
  public void setUp() throws Exception {
    tree = new BinaryTree();
    BinaryTree.Node root = tree.getRoot();
    root.setData(1);

    BinaryTree.Node n2 = new BinaryTree.Node(2);
    BinaryTree.Node n3 = new BinaryTree.Node(3);
    root.setChilds(n2, n3);


    BinaryTree.Node n4 = new BinaryTree.Node(4);
    BinaryTree.Node n5 = new BinaryTree.Node(5);
    n2.setChilds(n4, n5);

    BinaryTree.Node n6 = new BinaryTree.Node(6);
    n3.setChilds(n6, null);

    BinaryTree.Node n7 = new BinaryTree.Node(7);
    n4.setChilds(n7, null);

    BinaryTree.Node n8 = new BinaryTree.Node(8);
    BinaryTree.Node n9 = new BinaryTree.Node(9);
    n6.setChilds(n8, n9);
  }

  @Test
  public void testSize() {
    Assert.assertEquals(tree.size(), 9);
    Assert.assertEquals(tree.sizeDfs(), 9);
    Assert.assertEquals(tree.sizeBfs(), 9);
  }

  @Test
  public void testHeight() {
    Assert.assertEquals(tree.height(), 4);
    Assert.assertEquals(tree.heightBfs(), 4);
  }
}
