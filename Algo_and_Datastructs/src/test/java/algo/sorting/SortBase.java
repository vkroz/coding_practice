package algo.sorting;

public abstract class SortBase {

  abstract public void sort(Comparable[] ar);

  boolean less(Comparable l, Comparable r) {
    return l.compareTo(r) < 0;
  }

  void exch(Comparable[] ar, int i, int j) {
    if (ar[i] != ar[j]) {
      Comparable t = ar[j];
      ar[j] = ar[i];
      ar[i] = t;

    }
  }

  void show(Comparable[] ar) {
    for (Comparable n : ar) {
      System.out.print(n + " ");
    }
    System.out.println("");
  }

  public boolean isSorted(Comparable[] a) {  // Test whether the array entries are in order.
    for (int i = 1; i < a.length; i++)
      if (less(a[i], a[i - 1])) return false;
    return true;
  }


}
