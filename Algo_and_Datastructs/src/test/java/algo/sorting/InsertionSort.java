package algo.sorting;

/**
 * One of the simplest segwick.hackerrank.algorithms.sorting hackerrank.algorithms works as follows: First, find the smallest item in the
 * array and exchange it with the first entry (itself if the first entry is already the smallest).
 * Then, find the next smallest item and exchange it with the sec- ond entry. Continue in this way
 * until the entire array is sorted. This method is called selection sort because it works by
 * repeatedly selecting the smallest remaining item.Created by vkroz on 4/2/16.
 */
public class InsertionSort extends SortBase {

  public static void main(String[] args) {
    SortBase app = new InsertionSort();
    app.sort(new Integer[]{4, 3, 1, 5, 6, 2});
    app.sort(new Integer[]{2, 4, 6, 8, 3});
    app.sort(new Integer[]{2, 3, 4, 5, 6, 7, 8, 9, 10, 1});
  }

  public void sort(Comparable[] ar) {

    show(ar);
    for (int i = 1; i < ar.length; i++) {
      for (int j = i; j > 0 && less(ar[j], ar[j-1]); j--) {
        exch(ar, j, j-1);
      }
      show(ar);
    }
    System.out.println("------------");

  }

}
