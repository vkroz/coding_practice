package algo.sorting;

/**
 * Top Down merge sort
 */
public class MergeSortTopDown extends SortBase {

  void merge(Comparable[] ar, int lo, int mid, int hi) {
    int i = lo, j = mid + 1;

    Comparable[] aux = new Comparable[ar.length];

    for (int k = lo; k <= hi; k++)  // Copy a[lo..hi] to aux[lo..hi].
      aux[k] = ar[k];
    for (int k = lo; k <= hi; k++)  // Merge back to a[lo..hi].
      if (i > mid) ar[k] = aux[j++];
      else if (j > hi) ar[k] = aux[i++];
      else if (less(aux[j], aux[i])) ar[k] = aux[j++];
      else ar[k] = aux[i++];
  }

  public void sort(Comparable[] ar) {
    sort(ar, 0, ar.length-1);
  }

  /**
   * Top-down merge sort
   * @param ar
   * @param lo
   * @param hi
   */
  void sort(Comparable[] ar, int lo, int hi) {
    if(hi <= lo) return;

    int mid = lo + (hi-lo)/2;
    sort(ar, lo, mid);
    sort(ar, mid+1, hi);
    merge(ar, lo, mid, hi);
  }
}
