package algo.sorting;

/**
 * One of the simplest segwick.hackerrank.algorithms.sorting hackerrank.algorithms works as follows: First, find the smallest item in the
 * array and exchange it with the first entry (itself if the first entry is already the smallest).
 * Then, find the next smallest item and exchange it with the sec- ond entry. Continue in this way
 * until the entire array is sorted. This method is called selection sort because it works by
 * repeatedly selecting the smallest remaining item.Created by vkroz on 4/2/16.
 */
public class SelectionSort extends SortBase {
  public static void main(String[] args) {
    SortBase app = new SelectionSort();
    app.sort(new Integer[]{4, 3, 1, 5, 6, 2});
    System.out.println("------------");
    app.sort(new Integer[]{2, 4, 6, 8, 3});
    System.out.println("------------");
    app.sort(new Integer[]{2, 3, 4, 5, 6, 7, 8, 9, 10, 1});
    System.out.println("------------");
  }

  private static void printArray(int[] ar) {
    for (int n : ar) {
      System.out.print(n + " ");
    }
    System.out.println("");
  }

  @Override
  public void sort(Comparable[] ar) {

    for (int j = 0; j < ar.length-1; j++) {
      // First, find the smallest item in the array and exchange it with the first entry
      // (itself if the first entry is already the smallest).
      int min_idx = j;

      for (int i = j+1; i < ar.length; i++) {
        if ( this.less(ar[i], ar[min_idx]))
          min_idx = i;
      }
      exch(ar, j, min_idx);
      show(ar);
    }

  }

}
