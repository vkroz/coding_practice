package algo.sorting;

import java.util.Arrays;
import java.util.List;

/**
 * Quicksort is complementary to mergesort: for mergesort, we break the array into two subarrays to
 * be sorted and then combine the ordered subarrays to make the whole ordered array; for quicksort,
 * we rearrange the array such that, when the two subarrays are sorted, the whole array is ordered.
 * In the first instance, we do the two recursive calls before working on the whole array; in the
 * second instance, we do the two recursive calls after working on the whole array. For mergesort,
 * the array is divided in half; for quicksort, the position of the partition depends on the
 * contents of the array.
 */
public class QuickSort extends SortBase {


  public void sort(Comparable[] a) {
    List<Comparable> l = Arrays.asList(a);
//    Collections.shuffle(l);
    a = l.toArray(a);
    sort(a, 0, a.length - 1);
  }


  void sort(Comparable[] a, int lo, int hi) {
    if (hi <= lo) return;
    int j = partition(a, lo, hi);  // Partition (see page 291).
    sort(a, lo, j - 1);            // Sort left part a[lo .. j-1].
    sort(a, j + 1, hi);            // Sort right part a[j+1 .. hi].

  }

  private int partition(Comparable[] a, int lo, int hi) {

    // Partition into a[lo..i-1], a[i], a[i+1..hi].
    int i = lo, j = hi + 1;          // left and right scan indices
    Comparable v = a[lo];            // partitioning item

    while (true) {  // Scan right, scan left, check for scan complete, and exchange.
      while( less(a[++i], v) )
        if (i == hi)
          break;
      while (less(v, a[--j]))
        if (j == lo)
          break;
      if (i >= j)
        break;
      exch(a, i, j);
    }
    exch(a, lo, j);                   // Put v = a[j] into position
    return j;                         // with a[lo..j-1] <= a[j] <= a[j+1..hi].
  }

  private void swap(Comparable[] a, int k, int firstGreater) {
    Comparable t = a[k];
    a[k] = a[firstGreater];
    a[firstGreater] = t;
  }

}
