package algo.sorting;

/**
 * Bottom Up merge sort
 */
public class MergeSortBottomUp extends SortBase {


  Comparable[] aux;

  /**
   * merging the subarrays a[lo..mid] with a[mid+1..hi] into a single ordered array, leaving the
   * result in a[lo..hi].
   */
  void merge(Comparable[] ar, int lo, int mid, int hi) {
    int i = lo, j = mid + 1;


    for (int k = lo; k <= hi; k++)  // Copy a[lo..hi] to aux[lo..hi].
      aux[k] = ar[k];
    for (int k = lo; k <= hi; k++)  // Merge back to a[lo..hi].
      if (i > mid) ar[k] = aux[j++];
      else if (j > hi) ar[k] = aux[i++];
      else if (less(aux[j], aux[i])) ar[k] = aux[j++];
      else ar[k] = aux[i++];
  }

  public void sort(Comparable[] ar) {
    int N = ar.length;
    aux = new Comparable[N];

    for (int sz = 1; sz < N; sz += sz) {
      for (int lo = 0; lo < N - sz; lo += sz * 2) {
        int mid = lo + sz - 1;
        int hi = Math.min(lo + sz + sz - 1, N - 1);
        merge(ar, lo, mid, hi);
        System.out.printf("size=%d lo=%d mid=%d hi=%d\n", sz, lo, mid, hi);
      }
      System.out.println("");
    }

  }


}
