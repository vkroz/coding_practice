package basic_math;

import org.junit.Assert;
import org.junit.Test;

public class GreatestCommonDivisor {
    public float gcd(int a, int b) {
        if (b == 0) {
            return (float) a;
        } else {
            return gcd(b, a % b);
        }
    }


    @Test public void testGcd() {
        Assert.assertEquals(2.0, gcd(6, 4), 0.0001);
        Assert.assertEquals(1.0, gcd(1, 3), 0.0);
        Assert.assertEquals(1.0, gcd(5, 7), 0.0);
        Assert.assertEquals(1.0, gcd(3, 2), 0.0);
        Assert.assertEquals(5.0, gcd(15, 5), 0.0);
        Assert.assertEquals(5.0, gcd(5, 15), 0.0);
        Assert.assertEquals(4.0, gcd(16, 28), 0.0);
        Assert.assertEquals(3.0, gcd(12, 9), 0.0);
    }
}
