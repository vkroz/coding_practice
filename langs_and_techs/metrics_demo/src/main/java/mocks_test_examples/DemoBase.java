package metrics_demo;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class DemoBase {
    static protected final MetricRegistry metricsRegistry = new MetricRegistry();
    static protected final Random rnd = new Random(System.currentTimeMillis());

    static void startMetricsReporter() {
        ConsoleReporter reporter =
                ConsoleReporter.forRegistry(metricsRegistry).convertRatesTo(TimeUnit.SECONDS)
                        .convertDurationsTo(TimeUnit.MILLISECONDS).build();
        reporter.start(2, TimeUnit.SECONDS);
    }

    static void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
        }
    }

    static int randomInt(int bound) {
        return rnd.nextInt(bound);
    }
}
