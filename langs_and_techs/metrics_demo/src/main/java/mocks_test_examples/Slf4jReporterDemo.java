package metrics_demo;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.codahale.metrics.Slf4jReporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;


public class Slf4jReporterDemo extends DemoBase {
    static Logger logger = LoggerFactory.getLogger(Slf4jReporterDemo.class);
    static Logger metricsLogger = LoggerFactory.getLogger("MetricsReportLog");

    ScheduledReporter reporter;

    public static void main(String args[]) {
        try {
            new Slf4jReporterDemo().run(args);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Slf4jReporterDemo() {
        reporter = Slf4jReporter.forRegistry(metricsRegistry).convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS).outputTo(metricsLogger).build();
    }

    private void run(String[] args) throws InterruptedException {

        logger.info("Slf4jReporterDemo.run() start");

        int nThreads = 3;
        ExecutorService executor = Executors.newFixedThreadPool(nThreads);
        reporter.start(1, TimeUnit.SECONDS);
        for(int i=0; i<nThreads; i++) {
            executor.execute(new Worker(metricsRegistry));
            sleep(rnd.nextInt(4));
        }

        logger.info("Wait till workers completion and shutdown");
        executor.awaitTermination(20, TimeUnit.SECONDS);
        executor.shutdown();
    }

    static class Worker implements Runnable {
        Random rnd = new Random(System.currentTimeMillis());

        final MetricRegistry metricRegistry;
        String threadLabel;
        Meter eventsMeter;
        int counter = 20;

        public Worker(MetricRegistry metricRegistry) {
            this.metricRegistry = metricRegistry;
        }

        @Override public void run() {
            threadLabel = format("%s", Thread.currentThread().getName());
            eventsMeter = metricRegistry.meter(threadLabel + "_events");

            while( counter > 0 ) {
                myDummyProcess();
                counter--;
                sleep(rnd.nextInt(3));
            }
            System.out.println(format("%s Finished", threadLabel));
        }

        /**
         * Simulates some useful work with arbitrary amounts of data.
         * Our metrics demo should capture how much imaginary data we process
         */
        private void myDummyProcess() {
            long eventsReceived = rnd.nextInt(1000);
            eventsMeter.mark(eventsReceived);
        }
    }

}
