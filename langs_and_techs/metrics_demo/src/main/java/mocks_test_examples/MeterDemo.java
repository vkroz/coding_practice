package metrics_demo;

import com.codahale.metrics.Meter;

/**
 * A meter measures the rate of events over time (e.g., “requests per second”).
 * In addition to the mean rate, meters also track 1-, 5-, and 15-minute moving averages.
 */
public class MeterDemo extends DemoBase {

    public static void main(String args[]) {
        startMetricsReporter();

        // Create and register meter metric
        Meter volumeMeter = new Meter();
        metricsRegistry.register("xxx", volumeMeter);
        // Another way to create and register metrics
        Meter requests = metricsRegistry.meter("requests");

        for (int i = 0; i < 40; i++) {
            // Mark the occurrence of single event
            requests.mark();
            // Mark the occurrence of N events
            volumeMeter.mark(randomInt(10));

            sleep(1);
        }
    }

}
