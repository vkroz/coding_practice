package metrics_demo;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import metrics_demo.stuff.QueueManager;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class GaugeDemo extends DemoBase {

    public static void main(String[] args) {
        QueueManager queueManager = new QueueManager(100);

        DemoBase.metricsRegistry.register(MetricRegistry.name(QueueManager.class, "queue", "size"),
                new Gauge<Integer>() {
                    @Override public Integer getValue() {
                        return queueManager.getQueueSize();
                    }
                });

        startMetricsReporter();

        for(int i =0; i< 10; i++) {
            queueManager.add(randomInt(5));
            sleep(1);
            queueManager.remove(randomInt(5));
            sleep(1);
        }
    }

}


