package metrics_demo;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import metrics_demo.stuff.QueueManager;

import static com.codahale.metrics.MetricRegistry.name;

public class TimersDemo extends DemoBase {

    public static void main(String[] args) {
        QueueManager queueManager = new QueueManager(100);

        final Timer myTimer = metricsRegistry.timer(name(TimersDemo.class, "responses"));

        startMetricsReporter();

        for(int i =0; i< 10; i++) {
            final Timer.Context context = myTimer.time();
            someAction();
            context.stop();
        }
    }

    static void someAction() {
        sleep(randomInt(5));
    }
}


