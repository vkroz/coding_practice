package metrics_demo.stuff;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Sample class that implement primitive queue
 */
public class QueueManager {
    private final Queue<Integer> queue;

    public QueueManager(int initialSize) {
        this.queue = new ArrayDeque<>();
        this.add(initialSize);
    }

    public void add(int number) {
        for(int i=0; i< number; i++)
            queue.add(1);
    }
    public void remove(int number) {
        for(int i=0; i< number; i++)
            queue.remove(1);
    }
    public int getQueueSize() {
        return queue.size();
    }
}
