"""
Working with structured binary data
"""
import struct
import sys
import binascii


def sample1():
    print "--------- sample1 ---------"
    j = -1
    for i in range(1,10):
        print "Outer loop: i=%d, j=%d" % (i, j)

        for j in range(1,10):
            print "     Inner loop: i=%d, j=%d" % (i, j)
            if j%5==0:
                break


def main():
    sample1()

if __name__ == '__main__':
    sys.exit(main())

