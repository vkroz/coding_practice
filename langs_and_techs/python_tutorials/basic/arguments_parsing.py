import argparse
from argparse import ArgumentParser

"""
Command line parsing
"""


"""
Simplest example
-------------------------------------------------
"""

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                    help='an integer for the accumulator')
parser.add_argument('--sum', dest='accumulate', action='store_const',
                    const=sum, default=max,
                    help='sum the integers (default: find the max)')

args = parser.parse_args(['--sum', '7', '-1', '42'])
print(args.accumulate(args.integers))


"""
Parent / Child parsers
-------------------------------------------------
"""

