import sys

# Get exception details
try:
    f = open('myfile.txt')
    s = f.readline()
    i = int(s.strip())
except IOError as e:
    print "I/O error({0}): {1}".format(e.errno, e.strerror)
except ValueError:
    print "Could not convert data to an integer."
except (RuntimeError, TypeError, NameError) as e:
    print "Some error:", sys.exc_info()[0], sys.exc_info()[1]
except:
    print "Unexpected error:", sys.exc_info()[0]
    raise

# Execute code at the end regardless of exception
arg='aaa.txt'
try:
    f = open(arg, 'r')
except IOError:
    print 'cannot open', arg
else:
    print arg, 'has', len(f.readlines()), 'lines'
    f.close()
