"""
Working with structured binary data
"""
import struct
import sys
import binascii


def sample1():
    values = (123, 'ab', 1.23)
    s = struct.Struct('I 2s f')
    packed_data = s.pack(*values)
    ascii_data = binascii.hexlify(packed_data)

    print 'Original values:', values
    print 'Format string  :', s.format
    print 'Uses           :', s.size, 'bytes'
    print 'Packed Value   :', ascii_data
    # print 'HEX Value   :', binascii.b2a_hex(packed_data)
    # print 'Base64 Value   :', binascii.b2a_base64(packed_data)

    repacked_data = binascii.unhexlify(ascii_data)

    s = struct.Struct('I 2s f')
    unpacked_data = s.unpack(repacked_data)
    print 'Unpacked Values:', unpacked_data


def endianness():
    print "--------- endianness ---------"
    values = (1, 'ab', 2.7)
    print 'Original values:', values

    endianness = [
        ('@', 'native, native'),
        ('=', 'native, standard'),
        ('<', 'little-endian'),
        ('>', 'big-endian'),
        ('!', 'network'),
    ]

    for code, name in endianness:
        s = struct.Struct(code + ' I 2s f')
        packed_data = s.pack(*values)
        print
        print 'Format string  :', s.format, 'for', name
        print 'Uses           :', s.size, 'bytes'
        print 'Packed Value   :', binascii.hexlify(packed_data)
        print 'Unpacked Value :', s.unpack(packed_data)


def buffers():
    print "--------- buffers ---------"
    import struct
    import binascii

    s = struct.Struct('I 2s f')
    values = (1, 'ab', 2.7)
    print 'Original:', values

    print
    print 'ctypes string buffer'

    import ctypes
    b = ctypes.create_string_buffer(s.size)
    print 'Before  :', binascii.hexlify(b.raw)
    s.pack_into(b, 0, *values)
    print 'After   :', binascii.hexlify(b.raw)
    print 'Unpacked:', s.unpack_from(b, 0)

    print
    print 'array'

    import array
    a = array.array('c', '\0' * s.size)
    print 'Before  :', binascii.hexlify(a)
    s.pack_into(a, 0, *values)
    print 'After   :', binascii.hexlify(a)
    print 'Unpacked:', s.unpack_from(a, 0)



def main():
    sample1()
    endianness()
    buffers()

if __name__ == '__main__':
    sys.exit(main())
