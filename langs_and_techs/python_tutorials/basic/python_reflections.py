"""
Manipulating Python objects in runtime
"""

# class Fixture(FixtureBase):

class FixtureType(type):
    # pass
    def __getattr__(self, attr):
        print "__getattr__(): attr=" + attr
        return attr

    # def __getattribute__(self, attr):
    #     print "__getattribute__(): attr=" + attr
    #     return attr

class FixtureBase(object):
    __metaclass__ = FixtureType

class Fixture(FixtureBase):
    class Customer(FixtureBase):
        transport = "tcp"
        date_from = "-5d"
        date_to = "now"

    class Kafka(FixtureBase):
        kafka_raw_topic = 'premapped'
        kafka_preProcessed_topic = 'mapped'
        kafka_processed_topic = 'parsed'

    sleep_time = 20
    test_uuid = None



print Fixture.Customer.transport
print Fixture.Kafka.kafka_preProcessed_topic
print Fixture.sleep_time
print Fixture.test_uuid

Fixture.Customer.cid=1234
print Fixture.Customer.cid

# print Fixture.customer.transport
