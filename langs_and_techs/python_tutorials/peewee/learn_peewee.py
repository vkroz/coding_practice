from peewee import *
from datetime import date

db = SqliteDatabase('test.db')
class Person(Model):
    name = CharField()
    birthday = DateField()
    is_relative = BooleanField()

    class Meta:
        database = db


class Pet(Model):
    owner = ForeignKeyField(Person, related_name='pets')
    name = CharField()
    animal_type = CharField()

    class Meta:
        database = db

db.connect()
print db

# Person.create_table()
# Pet.create_table()

uncle_bob = Person(name='Bob', birthday=date(1960, 1, 15), is_relative=True)
uncle_bob.save()

grandma = Person(name='Grandma', birthday=date(1935, 3, 1), is_relative=True)
grandma.save()
herb = Person.create(name='Herb', birthday=date(1950, 5, 5), is_relative=False)


bob_kitty = Pet.create(owner=uncle_bob, name='Kitty', animal_type='cat')
herb_fido = Pet.create(owner=herb, name='Fido', animal_type='dog')
herb_mittens = Pet.create(owner=herb, name='Mittens', animal_type='cat')
herb_mittens_jr = Pet.create(owner=herb, name='Mittens Jr', animal_type='cat')

"""
>>> grandma = Person.select().where(Person.name == 'Grandma L.').get()
We can also use a shorthand:

>>> grandma = Person.get(Person.name == 'Grandma L.')
Lists of records
Let’s list all the people in the database:

>>> for person in Person.select():
    ...     print person.name, person.is_relative
...
Bob True
Grandma L. True
"""


for person in Person.SELECT():
    print person.name, person.pets.count(), 'pets'
    for pet in person.pets:
        print '    ', pet.name, pet.animal_type


for pet in Pet.select().where(Pet.animal_type == 'cat'):
    print pet.name, pet.owner.name