"""
Test request timeout
"""
import requests


try:
    url = 'http://www.google111.com'
    resp = requests.get(url, timeout=(5, 10))
except Exception as e:
    print e

    if resp.status_code != 404:
        resp.raise_for_status()


