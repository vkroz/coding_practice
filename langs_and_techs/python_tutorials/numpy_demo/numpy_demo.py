import numpy as np
from numpy.linalg import inv, qr
from numpy.matlib import randn
import sys


def dot_vectors(a, b):
    if len(a) != len(b):
        raise Exception('wrong dimensions')

    c = [0]*len(a)
    for i in range(0, len(a)):
        c[i] = a[i] * b[i]
    return c

def dot(A, B):
    if A.shape != B.T.shape:
        raise Exception('wrong dimensions')

    m,n = A.shape
    n,p = B.shape

    C = np.arange(4).reshape(2,2)

    for i in range(0, m):
        for j in range(0, p):
            C[i,j] = 0
            for k in range(0,n):
                C[i,j] = C[i,j] + A[i,k] * B[k,j]
    return C


"""             i k    k j                                             i k    k j
1 2 3   1 2   a[0,0]*b[0,0]+a[0,1]*b[1,0]+a[0,2]*b[2,0]=1+6+15=22    a[0,0]*b[0,1]+a[0,1]*b[1,1]+a[0,2]*b[2,1]=1*2+2*4+3*6=28    
4 5 6   3 4   ?      ?    
        5 6       
"""

def mult(A,a):
    B = np.array(A)
    n_rows, n_cols = B.shape
    for i in range(0,n_rows):
        for j in range(0,n_cols):
            B[i,j] = A[i,j]*a
    return B

def main():

    A = np.array([(1,2,3), (4,5,6)])
    B = np.array([(1,2), (3,4), (5,6)])

    C = dot(A,B)
    print(C)
    print(A.dot(B))


    # c = dot_vectors((1,2,3), (4,5,6))
    # print(c)
    # A = np.array([(1,2,3), (4,5,6)])
    # A1 = mult(A,2)
    # print(A1)



if __name__ == '__main__':
    sys.exit(main())
