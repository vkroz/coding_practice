package metrics_demo.powermock;

import java.util.HashMap;
import java.util.Map;

public class ClassUnderTest {

    private Map<Long, String> serviceRegistrations = new HashMap();

    public long methodToTest(String service) {
        final long id = IdGenerator.generateNewId();
        serviceRegistrations.put(id, service);
        return id;
    }


}

class IdGenerator {
    private static long cnt=0;
    public static long generateNewId() {
        return System.currentTimeMillis();
    }
}
