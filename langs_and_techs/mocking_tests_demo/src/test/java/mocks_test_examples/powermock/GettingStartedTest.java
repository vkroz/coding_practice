package metrics_demo.powermock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;

import static org.junit.Assert.assertEquals;


@RunWith(PowerMockRunner.class)
@PrepareForTest( { IdGenerator.class })
public class GettingStartedTest {

    @PrepareForTest( { IdGenerator.class })
    @Test
    public void testMethod() {
        PowerMockito.mockStatic(IdGenerator.class);
        Mockito.when(IdGenerator.generateNewId()).thenReturn(123456L);
        PowerMockito.verifyStatic();

        ClassUnderTest myObj = new ClassUnderTest();
        long id = myObj.methodToTest("aaa");
        assertEquals(id, 123456L);
    }



}
