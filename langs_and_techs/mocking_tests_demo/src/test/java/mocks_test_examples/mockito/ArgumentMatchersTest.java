package mocks_test_examples.mockito;


import org.junit.Test;

import java.util.LinkedList;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

/**
 * @see <a href="http://site.mockito.org/">http://site.mockito.org/</a>
 */
public class ArgumentMatchersTest {


    /**
     * and stub method calls
     */
    @Test public void argumentsMatchers() {
        LinkedList mockedList = mock(LinkedList.class);

        //stubbing using built-in anyInt() argument matcher
        when(mockedList.get(anyInt())).thenReturn("element");

        //stubbing using hamcrest (let's say isValid() returns your own hamcrest matcher):
        when(mockedList.contains(argThat(isValid()))).thenReturn(true);

        //following prints "element"
        System.out.println(mockedList.get(999));

        //you can also verify using an argument matcher
        verify(mockedList).get(anyInt());
    }

    private Object argThat(boolean valid) {
        return null;
    }

    boolean isValid(String myElement) {
        boolean retval = myElement.length() > 5;
        return retval;
    }

    boolean isValid() {
        boolean retval = true;
        return retval;
    }
}
