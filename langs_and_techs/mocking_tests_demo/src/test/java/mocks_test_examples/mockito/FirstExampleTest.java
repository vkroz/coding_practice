package mocks_test_examples.mockito;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @see <a href="http://site.mockito.org/">http://site.mockito.org/</a>
 */
public class FirstExampleTest {

    /**
     * now you can verify interactions
     */
    @Test public void verifyInteractions() {

        // mock creation
        List mockedList = mock(List.class);

        // using mock object - it does not throw any "unexpected interaction" exception
        mockedList.add("one");
        mockedList.clear();

        // selective, explicit, highly readable verification
        verify(mockedList).clear();
        verify(mockedList).add("one");


        // And this one will cause test error, because such interaction didn't actually happen
        verify(mockedList).add("two");
    }

    /**
     * and stub method calls
     */
    @Test public void stubMethodCalls() {

        // you can mock concrete classes, not only interfaces
        LinkedList mockedList = mock(LinkedList.class);

        // stubbing appears before the actual execution
        when(mockedList.get(0)).thenReturn("first");

        // the following prints "first"
        System.out.println(mockedList.get(0));

        // the following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999));
    }

}
