package mocks_test_examples.mockito;

import org.junit.Test;

import java.util.LinkedList;

import static org.mockito.Mockito.*;

/**
 * @see <a href="http://site.mockito.org/">http://site.mockito.org/</a>
 */
public class ClassStubbingExampleTest {


    /**
     * and stub method calls
     */
    @Test public void stubMethodCalls() {

        // you can mock concrete classes, not only interfaces
        LinkedList mockedList = mock(LinkedList.class);

        // stubbing appears before the actual execution
        when(mockedList.get(0)).thenReturn("first");

        // the following prints "first"
        System.out.println(mockedList.get(0));

        // the following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999));
    }

}
