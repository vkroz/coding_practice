package junit_samples;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by vkroz on 11/29/16.
 */
public class SpecificTest extends Base {

    @BeforeClass
    public static void beforeClass() {
        Base.beforeClass();
        System.out.println("Called: " + SpecificTest.class.getSimpleName() + ".beforeClass");
    }

    @Before
    public void beforeMethod() {
        System.out.println("Called: " + SpecificTest.class.getSimpleName() + ".beforeMethod");
    }

    @Test
    public void testStep() {
        System.out.println("Called: " + SpecificTest.class.getSimpleName() + ".testStep");
        assertTrue(2+2==4);
    }

}
