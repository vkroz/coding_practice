package junit_samples;

/**
 * BAse class for all tests
 */
public class Base {
    public static void beforeClass() {
        System.out.println("Called: " + Base.class.getSimpleName() + ".beforeClass");
    }
}
