package loggly;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class NioSample1 {
  public static void main(String[] args) {

    String currentDir = System.getProperty("user.dir");
    System.out.println("Current dir using System:" + currentDir);
//    System.exit(1);

    RandomAccessFile aFile = null;
    try {
      aFile = new RandomAccessFile("/Users/vkroz/proj/learning/netty-tutorial/src/test/resources/nio-data.txt", "rw");
      FileChannel inChannel = aFile.getChannel();

      ByteBuffer buf = ByteBuffer.allocate(48);

      int bytesRead = inChannel.read(buf);
      while (bytesRead != -1) {

        System.out.println("Read " + bytesRead);
        buf.flip();

        while (buf.hasRemaining()) {
          System.out.print((char) buf.get());
        }

        buf.clear();
        bytesRead = inChannel.read(buf);
      }
      aFile.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
