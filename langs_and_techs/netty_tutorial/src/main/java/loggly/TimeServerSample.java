package loggly;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;

public class TimeServerSample extends SimpleChannelHandler {

  SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

  public static void main(String[] args) throws Exception {

    ChannelFactory factory =
        new NioServerSocketChannelFactory(
            Executors.newCachedThreadPool(),
            Executors.newCachedThreadPool());

    ServerBootstrap bootstrap = new ServerBootstrap(factory);

    bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
      public ChannelPipeline getPipeline() {
        return Channels.pipeline(new TimeServerSample());
      }
    });

    bootstrap.setOption("child.tcpNoDelay", true);
    bootstrap.setOption("child.keepAlive", true);
    bootstrap.setOption("reuseAddress", true);
    System.out.println("--- listening on 0.0.0.0:8080");
    bootstrap.bind(new InetSocketAddress(8080));
  }


  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
    e.getCause().printStackTrace();
    e.getChannel().close();
  }


  String getStringDate() {
    return df.format(new Date());
  }

  int getIntDate() {
    return (int)(System.currentTimeMillis() / 1000L + 2208988800L);
  }

  @Override
  public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
    System.out.println("Request received");
    Channel ch = e.getChannel();
//    ChannelBuffer time = ChannelBuffers.buffer(100);
    ChannelBuffer time = ChannelBuffers.buffer(4);

    String strDate = this.getStringDate();
    System.out.println("Current time: " + strDate);
//    time.writeBytes(strDate.getBytes());

    int currTime = this.getIntDate();
    time.writeInt(currTime);

    ChannelFuture f = ch.write(time);
    f.addListener(new ChannelFutureListener() {
      public void operationComplete(ChannelFuture future) throws Exception {
        Channel ch = future.getChannel();
        ch.close();
      }
    });
  }
}


