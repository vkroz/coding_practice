public class SimpleChannelHandler
extends Object
implements ChannelUpstreamHandler, ChannelDownstreamHandler
A ChannelHandler which provides an individual handler method for each event type. This handler down-casts the received upstream or or downstream event into more meaningful sub-type event and calls an appropriate handler method with the down-cast event. For an upstream event, the names of the methods are identical to the upstream event names, as introduced in the ChannelEvent documentation. For a downstream event, the names of the methods starts with the name of the operation and ends with "Requested" (e.g. writeRequested.)
Please use SimpleChannelUpstreamHandler or SimpleChannelDownstreamHandler if you want to intercept only upstream or downstream events.

Overriding the handleUpstream and handleDownstream method

You can override the handleUpstream and handleDownstream method just like overriding an ordinary Java method. Please make sure to call super.handleUpstream() or super.handleDownstream() so that other handler methods are invoked properly:

public class MyChannelHandler extends SimpleChannelHandler {

     @Override
     public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {

         // Log all channel state changes.
         if (e instanceof ChannelStateEvent) {
             logger.info("Channel state changed: " + e);
         }

         super.handleUpstream(ctx, e);
     }

     @Override
     public void handleDownstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {

         // Log all channel state changes.
         if (e instanceof MessageEvent) {
             logger.info("Writing:: " + e);
         }

         super.handleDownstream(ctx, e);
     }
 }
Nested Class Summary

Nested classes/interfaces inherited from interface org.jboss.netty.channel.ChannelHandler
ChannelHandler.Sharable
Constructor Summary

Constructors 
Constructor and Description
SimpleChannelHandler() 
Method Summary

