val greeting = "Hello world"
printf("%s\n", greeting)
print("\n")


val a = Array(11, 1, 2, 3, 4)
a.reduceLeft( (x,y) => Math.max(x,y) )


a.reduceLeft( _ max _ )


def and(x: Boolean, y: => Boolean) = if(x) y else false


def or(x: Boolean, y: Boolean) = if(x) true else y

and(true, true)

and(true, false)

and(false, false)
println("---------")

or(true, true)

or(true, false)

or(false, false)


