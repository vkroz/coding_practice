/**
  * Created by vkroz on 2/19/17.
  */
object Sqrt extends App {

  def isGoodEnough(guess: Double, x: Double) = {
    ((guess * guess - x) / x).abs < 0.0001
  }

  def improve(guess: Double, x: Double) = {
    (guess + x / guess) / 2
  }


  def sqrtIter(guess: Double, x: Double): Double = {
    if (isGoodEnough(guess, x))
      guess
    else sqrtIter(improve(guess, x), x)

  }

  def sqrt(n: Double) = {
    sqrtIter(1, n)
  }

  println(sqrt(1e60))

}
