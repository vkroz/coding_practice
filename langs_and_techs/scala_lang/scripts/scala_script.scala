println("Hello, " + args(0) + ", from script!")

println("\nUse foreach instead --------")

args.foreach((arg: String) => println(arg))

println("\nshorter notation--------")

args.foreach(arg => println(arg))


println("\nand even shorter notation--------")

args.foreach(println)

println("\nand using for loop--------")

for(arg <- args) {
  println(arg)
}
