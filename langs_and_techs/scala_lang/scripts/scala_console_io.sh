#!/bin/sh
exec scala -savecompiled "$0" "$@"
!#

// write some text out to the user with Console.print
Console.println("Hello")

// Console is imported by default, so it's not really needed, just use println
println("World")

// readLine: lets you prompt the user and also read their command line input
val name = readLine("What's your name? ")

// readInt: read a simple Int
print("How old are you? ")
val age = readInt()

// you can also print output with printf
printf("Your name is %s and you are %d years old.\n", name, age)

// you can also use the Java Scanner class, if desired
val scanner = new java.util.Scanner(System.in)
print("Where do you live? ")
val input = scanner.nextLine()
print("You live in " + input)
