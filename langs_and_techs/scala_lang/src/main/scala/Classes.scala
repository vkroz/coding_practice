object Classes {

  def main(args: Array[String]) {
    val pt = new Point(1, 2)
    println(pt)
    pt.move(10, 10)
    println(pt)
  }

}

/**
  * Created by vkroz on 1/19/17.
  */
class Point(var x:Int, var y: Int) {
  def move(dx:Int, dy: Int): Unit = {
    x += dx
    y = y + dy
  }

  override def toString: String = String.format("(%d, %d)", Integer.valueOf(x), Integer.valueOf(y))
}
