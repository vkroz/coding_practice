abstract class AbsIterator {
  type T

  def hasNext: Boolean

  def next: T
}


trait RichIterator extends AbsIterator {
  def foreach(f: T => Unit) {
    while (hasNext)
      f(next)
  }
}

class StringIterator(s: String) extends AbsIterator {
  type T = Char
  private var i = 0

  def hasNext = i < s.length

  /**
    * This is very short form of function, which in Java-style would look like
    * <code>
    * def next(): T = {
    * val ch = s.charAt(i);
    * i += 1;
    * return ch
    * }
    * </code>
    */
  def next = {
    val ch = s charAt i; i += 1; ch
  }

}

object StringIteratorTest {

  def main(args: Array[String]) {
    class Iter extends StringIterator(args(0)) with RichIterator
    val iter = new Iter
    iter foreach println
  }
}


