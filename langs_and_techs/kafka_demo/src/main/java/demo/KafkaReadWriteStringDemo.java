package demo;

import demo.data.DummyItem;
import demo.data.DummyItemHelper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Properties;
import java.util.function.Consumer;

public class KafkaReadWriteStringDemo {

    static Path DATA_FILE_PATH;
    final static String TOPIC_NAME = "mytopic";
    final static String BOOTSTRAP_SERVERS = "localhost:9092";

    @BeforeClass public static void init() throws URISyntaxException {
        URL path = KafkaReadWriteStringDemo.class
                .getResource("/kafka_payload_samples/events-sample.txt");
        DATA_FILE_PATH = Paths.get(path.toURI());
    }

    @Test public void writeReadStringTest() {
        produceToKafka();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
        consumeFromKafka();
    }

    private void consumeFromKafka() {
        System.out.println("------ consumer starts --------");
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 10);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "DemoConsumer");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(KafkaReadWriteStringDemo.TOPIC_NAME));

        ConsumerRecords<String, String> records = consumer.poll(30000);
        for (ConsumerRecord<String, String> record : records) {
            System.out.println("> Consumed (" + record.key() + " : " + record.value() + ") at offset " + record.offset());
        }
        consumer.close();
        System.out.println("------ consumer finished --------");

    }

    public void produceToKafka() {


        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");


        try {
            Producer<String, String> producer = new KafkaProducer<>(props);
            Files.lines(DATA_FILE_PATH).forEach(new Consumer<String>() {
                @Override public void accept(String s) {

                    DummyItem item = DummyItemHelper.parseFromJson(s);
                    String key = item.getProductId();
                    String msg = item.toString();
                    producer.send(new ProducerRecord<String, String>(KafkaReadWriteStringDemo.TOPIC_NAME, key, msg));
                }
            });
            producer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
