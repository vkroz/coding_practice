package demo.data;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * Dummy item object
 */
public class DummyItem implements Serializable {
    String productId;
    String tenantId;
    Boolean isValid;

    public DummyItem(String productId, String tenantId, Boolean isValid) {
        this.productId = productId;
        this.tenantId = tenantId;
        this.isValid = isValid;
    }

    public DummyItem() {

    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Boolean getValid() {
        return isValid;
    }

    public void setValid(Boolean valid) {
        isValid = valid;
    }

    @Override public String toString() {
        return MoreObjects.toStringHelper(this).add("productId", productId).add("tenantId", tenantId)
                .add("isValid", isValid).toString();
    }
}
