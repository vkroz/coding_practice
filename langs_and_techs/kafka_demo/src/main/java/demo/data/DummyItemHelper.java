package demo.data;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;

/**
 * Helps to convert json line into dummy item  object
 */
public class DummyItemHelper implements Serializable {

    static private final ObjectMapper objectMapper = new ObjectMapper();

    static public DummyItem parseFromJson(String jsonStr) {
        DummyItem retval = null;
        try {
            JsonNode lineJson = objectMapper.readTree(jsonStr);
            JsonNode payload = objectMapper.readTree(lineJson.path("payload").asText());
            retval = new DummyItem();
            retval.setProductId(payload.path("product_id").asText());
            retval.setTenantId(payload.path("tenant_id").asText());
            retval.setValid(false);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return retval;
    }

}
