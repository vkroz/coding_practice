package demo;

import demo.data.DummyItem;
import demo.data.DummyItemHelper;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Properties;
import java.util.function.Consumer;

/**
 * Key design points of using avro binary encoding of kafka messages
 * 1. Add Avro dependency to pom.xml
 * 2. Get avro schema
 * 3. Producing: message key must by array of bytes. The following steps to achieve this
 * 3.1 set value.serializer=org.apache.kafka.common.serialization.ByteArraySerializer
 * 3.2 Create producer as `Producer<String, byte[]> producer`
 */
public class KafkaReadWriteAvroDemo {

    final static String TOPIC_NAME = "mytopic";
    final static String BOOTSTRAP_SERVERS = "localhost:9092";
    static Path DATA_FILE_PATH;
    static Schema avroSchema;

    @BeforeClass public static void init() throws URISyntaxException {
        URL path = KafkaReadWriteAvroDemo.class
                .getResource("/kafka_payload_samples/events-sample.txt");
        DATA_FILE_PATH = Paths.get(path.toURI());

        // Get avro schema
        InputStream in = KafkaReadWriteAvroDemo.class.getResourceAsStream("/dummy_item.avsc");
        try {
            avroSchema = new Schema.Parser().parse(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test public void writeReadStringTest() {
        produceToKafka();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
        consumeFromKafka();
    }

    private void consumeFromKafka() {
        System.out.println("------ consumer starts --------");
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "DemoConsumer");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 10);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.ByteArrayDeserializer");

        KafkaConsumer<String, byte[]> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(KafkaReadWriteAvroDemo.TOPIC_NAME));

        ConsumerRecords<String, byte[]> records = consumer.poll(30000);
        for (ConsumerRecord<String, byte[]> record : records) {

            // Deserialize users from disk
            GenericDatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(avroSchema);
            byte[] byteData = record.value();
            try (ByteArrayInputStream  byteArrayInputStream = new ByteArrayInputStream(byteData)) {
                Decoder decoder = DecoderFactory.get().binaryDecoder(byteArrayInputStream, null);

                GenericRecord avroRecord = datumReader.read(null, decoder);
                System.out.println(
                        "> Consumed Avro (" + record.key() + " : " + avroRecord.toString() + ") at offset " + record
                                .offset());

            } catch (AvroRuntimeException e) {
                // let's print payload as string if avro unable to deserialize properly
                System.out.println(
                        "> Consumed String (" + record.key() + " : " + record.value().toString() + ") at offset " + record
                                .offset());

            }
            catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {
            consumer.commitSync();
        } catch (CommitFailedException e) {
            e.printStackTrace();
        }

        consumer.close();
        System.out.println("------ consumer finished --------");

    }

    public void produceToKafka() {
        System.out.println("------ producer starts --------");


        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.CLIENT_ID_CONFIG, KafkaReadWriteAvroDemo.class.getSimpleName());
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.ByteArraySerializer");


        try {
            Producer<String, byte[]> producer = new KafkaProducer<>(props);
            Files.lines(DATA_FILE_PATH).forEach(new Consumer<String>() {
                @Override public void accept(String s) {
                    DummyItem item = DummyItemHelper.parseFromJson(s);
                    String key = item.getProductId();
                    GenericData.Record avroRecord = new GenericData.Record(avroSchema);
                    avroRecord.put("request_id", "xxx");
                    avroRecord.put("product_id", item.getProductId());
                    avroRecord.put("isPriority", true);
                    avroRecord.put("isRetry", false);

                    try (ByteArrayOutputStream outs = new ByteArrayOutputStream()) {
                        GenericDatumWriter<GenericRecord> writer =
                                new GenericDatumWriter<GenericRecord>(avroSchema);
                        Encoder e = EncoderFactory.get().binaryEncoder(outs, null);
                        writer.write(avroRecord, e);
                        e.flush();
                        byte[] byteData = outs.toByteArray();

                        producer.send(new ProducerRecord<String, byte[]>(
                                KafkaReadWriteAvroDemo.TOPIC_NAME, key, byteData));

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }


                }
            });
            producer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("------ producer finished --------");
    }

}
