package zk_samples;

import org.apache.log4j.Logger;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;

public class ListZkNodes implements Watcher {
    final static Logger logger = Logger.getLogger(ListZkNodes.class);
    private final String zkConn;
    private ZooKeeper zk;

    public ListZkNodes(String zkConn) {
        this.zkConn = zkConn;
    }

    public static void main(String[] args) throws Exception {
        logger.info("------ start ------");
        ListZkNodes app = new ListZkNodes("localhost:2181");
        app.startZk();
        // Create Zk cliet
        logger.info("------ end ------");
    }

    private void startZk() throws IOException {
        this.zk = new ZooKeeper(this.zkConn, 15000, this);
    }

    @Override
    public void process(WatchedEvent event) {
        logger.info("process event: " + event);
    }
}
