package spark.accumulators;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.StorageLevels;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.util.LongAccumulator;
import scala.Serializable;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Demo of generating and colecting custom metrics from distributed tasks
 * Submit input for the job using `$ nc -lk 9999` command
 */
public final class StandardAccumulatorDemo {

    @org.junit.Test public void testMain() throws Exception {
        System.setProperty("spark.app.name", "streaming.NetworkWordCount");
        System.setProperty("spark.master", "local[*]");
        StandardAccumulatorDemo.run("localhost", 9999);
        System.out.println("=======================================");
    }

    public static void run(String hostname, int port) throws Exception {

        // ------- Prepare --------
        setStreamingLogLevels();
        SparkConf sparkConf = new SparkConf().setAppName("StandardAccumulatorDemo");

        // ------- run  --------
        (new DriverFlow(hostname, port)).run(sparkConf, Durations.seconds(5));
    }

    private static void setStreamingLogLevels() {
        boolean log4jInitialized = Logger.getRootLogger().getAllAppenders().hasMoreElements();
        if (!log4jInitialized) {
            // We first log something to initialize Spark's default logging, then we override the
            // logging level.
            Logger.getRootLogger().info("Setting log level to [WARN] for streaming example."
                    + " To override add a custom log4j.properties to the classpath.");
            Logger.getRootLogger().setLevel(Level.WARN);
        }
    }


    static class DriverFlow implements Serializable {
        private static final Pattern SPACE = Pattern.compile(" ");

        private final String hostname;
        private final int port;

        LongAccumulator accum1;
        LongAccumulator accum2;

        public DriverFlow(String hostname, int port) {
            this.hostname = hostname;
            this.port = port;

        }


        public void run(SparkConf sparkConf, Duration seconds) throws InterruptedException {
            JavaStreamingContext javaStreamingContext = new JavaStreamingContext(sparkConf, Durations.seconds(5));

            accum1 = javaStreamingContext.sparkContext().sc().longAccumulator();
            accum2 = javaStreamingContext.sparkContext().sc().longAccumulator();

            JavaReceiverInputDStream<String> inputStream =
                    javaStreamingContext.socketTextStream(hostname, port, StorageLevels.MEMORY_AND_DISK_SER);

            distributedRun(inputStream);
            javaStreamingContext.start();
            javaStreamingContext.awaitTermination();
        }

        public void distributedRun(JavaReceiverInputDStream<String> inputStream) {


            JavaDStream<String> words = inputStream.flatMap(new FlatMapFunction<String, String>() {
                @Override public Iterator<String> call(String x) {
                    List<String> words = Arrays.asList(SPACE.split(x));
                    return words.iterator();
                }
            });
            JavaPairDStream<String, Integer> wordCounts =
                    words.mapToPair(new PairFunction<String, String, Integer>() {
                        @Override public Tuple2<String, Integer> call(String s) {
                            accum1.add(1);
                            return new Tuple2<>(s, 1);
                        }
                    }).reduceByKey(new Function2<Integer, Integer, Integer>() {
                        @Override public Integer call(Integer i1, Integer i2) {
                            accum2.add(1);
                            return i1 + i2;
                        }
                    });

            wordCounts.print();
            wordCounts.foreachRDD(new VoidFunction<JavaPairRDD<String, Integer>>() {
                @Override public void call(JavaPairRDD<String, Integer> stringIntegerJavaPairRDD)
                        throws Exception {
                    System.out.println("# of items in RDD=" + stringIntegerJavaPairRDD.count());
                    System.out.println("Map() calls      =" + accum1.value());
                    System.out.println("Reduce() calls   =" + accum2.value());
                    accum1.reset();
                    accum2.reset();
                }
            });
        }


    }

}


