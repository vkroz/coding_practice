package spark.custom_metrics;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.StorageLevels;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Serializable;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.regex.Pattern;

/**
 * Demo of generating and colecting custom metrics from distributed tasks
 * Submit input for the job using `$ nc -lk 9999` command
 */
public final class CustomMetricsDemo {

    @org.junit.Test public void testMain() throws Exception {
        System.setProperty("spark.app.name", "streaming.NetworkWordCount");
        System.setProperty("spark.master", "local[*]");
        CustomMetricsDemo.run("localhost", 9999);
        System.out.println("=======================================");
    }

    public static void run(String hostname, int port) throws Exception {

        // ------- Prepare --------
        setStreamingLogLevels();
        SparkConf sparkConf = new SparkConf().setAppName("CustomMetricsDemo");
        JavaStreamingContext ssc = new JavaStreamingContext(sparkConf, Durations.seconds(5));

        // ------- run  --------
        JavaReceiverInputDStream<String> inputStream = ssc.socketTextStream(
                hostname, port, StorageLevels.MEMORY_AND_DISK_SER);
        (new DriverFlow()).distributedRun(inputStream);
        ssc.start();
        ssc.awaitTermination();
    }

    private static void setStreamingLogLevels() {
            boolean log4jInitialized = Logger.getRootLogger().getAllAppenders().hasMoreElements();
            if (!log4jInitialized) {
                // We first log something to initialize Spark's default logging, then we override the
                // logging level.
                Logger.getRootLogger().info("Setting log level to [WARN] for streaming example." +
                        " To override add a custom log4j.properties to the classpath.");
                Logger.getRootLogger().setLevel(Level.WARN);
            }
        }

}

class DriverFlow implements Serializable {
    private static final Pattern SPACE = Pattern.compile(" ");


    public void distributedRun(JavaDStream<String> inputStream) {
        JavaDStream<String> words = inputStream.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterator<String> call(String x) {
                return Arrays.asList(SPACE.split(x)).iterator();
            }
        });
        JavaPairDStream<String, Integer> wordCounts = words.mapToPair(
                new PairFunction<String, String, Integer>() {
                    @Override
                    public Tuple2<String, Integer> call(String s) {
                        return new Tuple2<>(s, 1);
                    }
                }).reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer i1, Integer i2) {
                return i1 + i2;
            }
        });

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>");
        wordCounts.print();
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>");

    }
}
