package demo.kryo;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;


/**
 * SerDe Kryo demo
 * Read JSON data from file into PoJo class
 * Serialize class and write into temp file
 * <p>
 * Reconstruct PoJo class instance by deserializing from binary file
 * Print content
 */
public class KryoDemo {


    public static void main(String[] args) throws IOException, URISyntaxException {
        KryoDemo app = new KryoDemo();
        app.run();
    }

    private void run() throws URISyntaxException, IOException {
        ClassLoader classLoader = KryoDemo.class.getClassLoader();
        URL jsonDataUrl = classLoader.getResource("payload.json");
        Path jsonDataPath = Paths.get(jsonDataUrl.toURI());
        System.out.println(jsonDataPath);


        ObjectMapper objectMapper = new ObjectMapper();
        Kryo kryo = new Kryo();

        Files.lines(jsonDataPath).forEach((line) -> {

            try {
                // Read from Json
                MyData myData = objectMapper.readValue(line, MyData.class);
                System.out.println("Object loaded from Json: " + myData);

                // Serialize
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                Output output = new Output(bos);
                kryo.writeObject(output, myData);
                output.close();


                byte[] bytes = bos.toByteArray();
                System.out.println("Serialized data " + Arrays.toString(bytes));


                // Desrialize
                ByteArrayInputStream bin = new ByteArrayInputStream(bytes);
                Input input = new Input(bin);
                MyData deserData = kryo.readObject(input, MyData.class);
                input.close();
                System.out.println("Deserialized object " + deserData);


                assert myData.equals(deserData);

            } catch (IOException e) {
                e.printStackTrace();
            }

        });


    }

}
