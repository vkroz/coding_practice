package demo.kryo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.google.common.base.MoreObjects;

import java.util.Map;

/**
 * Sample POJO class
 * <pre><code>
 *     {
 *        "type": "PRODUCT",
 *        "status": "PRODUCT_PROCESSED",
 *        "product_id": "7IANCA4Y766D",
 *        "pcf_version": 1,
 *        "product_version": 1490307585680,
 *        "meta": {
 *            "request_batch_id": "c288b887-5087-4ffa-add7-f62278df55ed",
 *            "product_class_type": "REGULAR",
 *            "qid_list": "MARKETPLACE_PARTNER"
 *        },
 *        "added": "Thu Dec 01 15:37:27 UTC 2016"
 *     }
 * </code></pre>
 * }
 */
public class MyData {
    String type;
    String status;
    String productId;
    Integer pcfVersion;
    Long productVersion;
    Map<String, String> meta;
    String added;

    @Override public String toString() {
        return MoreObjects.toStringHelper(this).add("type", type).add("status", status)
                .add("product_id", productId).add("pcf_version", pcfVersion)
                .add("productVersion", productVersion).add("meta", meta).add("added", added)
                .toString();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductId() {
        return productId;
    }

    @JsonSetter("product_id")
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getPcfVersion() {
        return pcfVersion;
    }

    @JsonSetter("pcf_version")
    public void setPcfVersion(Integer pcfVersion) {
        this.pcfVersion = pcfVersion;
    }

    public Long getProductVersion() {
        return productVersion;
    }

    @JsonSetter("product_version")
    public void setProductVersion(Long productVersion) {
        this.productVersion = productVersion;
    }

    public Map<String, String> getMeta() {
        return meta;
    }

    public void setMeta(Map<String, String> meta) {
        this.meta = meta;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }
}
