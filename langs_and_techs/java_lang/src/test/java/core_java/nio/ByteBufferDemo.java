package core_java.nio;

import core_java.TestBase;
import org.junit.Test;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.Arrays;

import static java.lang.String.format;
import static java.lang.String.valueOf;

/**
 * How to use nio.BytesBuffer
 *
 * @see "https://docs.oracle.com/javase/7/docs/api/java/nio/ByteBuffer.html"
 * @see "http://tutorials.jenkov.com/java-nio/buffers.html"
 * @see "http://cs.brown.edu/courses/cs161/papers/j-nio-ltr.pdf"
 */
public class ByteBufferDemo extends TestBase {

    @Test
    public void markPositionDemo() {
        try {
            byte[] bytes = Files.readAllBytes(DATA_FILE_PATH);

            // Just add 10 bytes to buffer
            ByteBuffer buffer = ByteBuffer.allocate(100);
            System.out.println(buffer.toString());
            for (int i = 0; i < 10; i++) {
                buffer.put(bytes[i]);
            }
            System.out.println(buffer.toString());

            // Now set mark
            System.out.println("\nSet mark on pos=10, and rewind position to 20");
            buffer.mark().position(20);
            System.out.println(buffer.toString());


            // add another 10 bytes to buffer
            System.out.println("\nAdd another 10 bytes to buffer");
            for (int i = 0; i < 10; i++) {
                buffer.put(bytes[i]);
            }
            System.out.println(buffer.toString());


            System.out.println("\nReset, watch that position now reset to mark");
            buffer.reset();
            System.out.println(buffer.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void fillingDemo() {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer
                .put((byte) 'H')
                .put((byte) 'e')
                .put((byte) 'l')
                .put((byte) 'l')
                .put((byte) 'o');

        System.out.println(buffer.toString());
        System.out.println(Arrays.toString(buffer.array()));

        // Now let's modifiy buffer content
        buffer.put(0, (byte)'M').put((byte)'w');

        System.out.println(buffer.toString());
        System.out.println(Arrays.toString(buffer.array()));

    }

    @Test
    public void flippingDemo() {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer
                .put((byte) 'H')
                .put((byte) 'e')
                .put((byte) 'l')
                .put((byte) 'l')
                .put((byte) 'o');

        System.out.println(buffer.toString());
        System.out.println(Arrays.toString(buffer.array()));

        // Now let's modifiy buffer content
        buffer.put(0, (byte)'M').put((byte)'w');

        System.out.println(buffer.toString());
        System.out.println(Arrays.toString(buffer.array()));

    }

}
