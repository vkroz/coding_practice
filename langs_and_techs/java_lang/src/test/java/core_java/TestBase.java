package core_java;

import org.junit.BeforeClass;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TestBase {

    public static Path DATA_FILE_PATH;

    @BeforeClass public static void init() throws URISyntaxException {
        URL path = TestBase.class.getResource("/data_samples/data_sample.txt");
        DATA_FILE_PATH = Paths.get(path.toURI());
    }

}
