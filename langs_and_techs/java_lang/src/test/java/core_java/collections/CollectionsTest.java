package core_java.collections;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class CollectionsTest {

    /**
     * Demonstarte: Java collections can contain null as value
     */
    @Test
    public void addNullToCollection() {
        List<String> stringList = new ArrayList<>();

        stringList = Arrays.asList(new String[] {"xxx", null, "zzz"});
        Assert.assertEquals(stringList.get(0), "xxx");
        Assert.assertEquals(stringList.get(1), null);
        Assert.assertEquals(stringList.get(2), "zzz");

        Map<String, String> stringMap = new HashMap<>();
        stringMap.put("aaa", "aaa");
        stringMap.put("bbb", null);
        stringMap.put("ccc", "ccc");
        Assert.assertEquals(stringMap.get("aaa"), "aaa");
        Assert.assertEquals(stringMap.get("bbb"), null);
        Assert.assertEquals(stringMap.get("ccc"), "ccc");
    }
}
