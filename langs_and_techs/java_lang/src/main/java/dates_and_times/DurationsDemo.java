package dates_and_times;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.Duration;

public class DurationsDemo {

    static public void main(String[] args) throws InterruptedException {
        new DurationsDemo().run();
    }

    private void run() throws InterruptedException {

        DateTime startTime = new DateTime (DateTimeUtils.currentTimeMillis());
        Thread.sleep(3000);
        DateTime finishTime = new DateTime (DateTimeUtils.currentTimeMillis());
        Duration duration = new Duration(startTime, finishTime);
        System.out.printf("Time elapsed: %d seconds\n", duration.getStandardSeconds());


        long startTimeMillis = DateTimeUtils.currentTimeMillis();
        Thread.sleep(3000);
        long finishTimeMillis = DateTimeUtils.currentTimeMillis();
        long seconds = Duration.millis(startTimeMillis-finishTimeMillis).getStandardSeconds();
        System.out.printf("Time elapsed: %d seconds\n", duration.getStandardSeconds());

    }


}
