package dates_and_times;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.*;

import static java.lang.String.format;

/**
 * All kinds o manipulations with dates
 */
public class JodaDatesAndTimes {

    public static void main(String[] args) {
        JodaDatesAndTimes app = new JodaDatesAndTimes();
        app.systemTime();
        app.measureInterval();
    }

    private void systemTime() {
    }

    private static void measureInterval() {
        DateTime startTime = DateTime.now();

        System.out.println(format("Current time: %s", startTime));
        System.out.println(format("Current epoch timestamp: %s", DateTime.now().getMillis()));
        System.out.println(format("Current systemtime: %s", System.currentTimeMillis()));

        Random rnd = new Random(System.currentTimeMillis());
        int waitTimeSec = rnd.nextInt(5);

        System.out.println(format("Wait for %s seconds", waitTimeSec));
        sleep(waitTimeSec);

        DateTime endTime = DateTime.now();
        System.out.println(format("End at %s", endTime));

        Duration duration = new Duration(startTime, endTime);
        System.out.println(format("Duration is %s seconds", duration.getStandardSeconds()));
    }

    private static void sleep(int waitTimeSec) {
        try {
            Thread.sleep(waitTimeSec * 1000);
        } catch (InterruptedException e) {
        }
    }
}
