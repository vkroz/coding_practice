import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.*;

public class Strings {

    @Test public void splitString() {
        String str = "{\"hostName\":\"172.28.90.55\",\"ipAddress\":\"172.28.90.55\",\"procId\":14001, threadPriority: 5}";
        String pattern = "\\{|:|,| |\\}";
        List<String> arr = asList(str.split( pattern));

        Assert.assertEquals("5", arr.get(10));

    }
}
