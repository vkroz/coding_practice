package functional_interfaces;

/**
 * Created by vkroz on 12/20/16.
 */
class Person {
    final String name;
    final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void printPerson() {
        System.out.println(name);
    }
}
