package functional_interfaces;

/**
 * Created by vkroz on 12/20/16.
 */
interface Checker {
    boolean test(Person p);
}
