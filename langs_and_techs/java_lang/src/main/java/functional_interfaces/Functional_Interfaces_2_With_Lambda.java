package functional_interfaces;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Functional_Interfaces_2_With_Lambda {
    ;;

    static public void main(String... args) {
        List<Person> persons = Arrays.asList(new Person("Vasya", 20), new Person("Petya", 30), new Person("Dedulya", 120));
        printPersons(persons, p -> p.age >= 30);

        // Even more - we can specifiy different processing on person
        System.out.println("Who's yonger 40?");
        processPersons(persons, p -> p.age < 100, p -> p.printPerson());

    }

    public static void printPersons(
            List<Person> roster, Checker tester) {
        for (Person p : roster) {
            if (tester.test(p)) {
                p.printPerson();
            }
        }
    }

    // Flexible processing using built-in functional interfaces
    public static void processPersons(
            List<Person> roster,
            Predicate<Person> tester,
            Consumer<Person> block) {
        for (Person p : roster) {
            if (tester.test(p)) {
                block.accept(p);
            }
        }
    }


}
