package functional_interfaces;

import java.util.Arrays;
import java.util.List;

public class Functional_Interfaces_1_Old_Style {

    ;

    static class CheckPerson implements Checker {
        @Override public boolean test(Person p) {
            return true;
        }
    };

    static public void main(String... args) {
        List<Person> persons = Arrays.asList(new Person("Vasya", 20), new Person("Petya", 30), new Person("Dedulya", 120));
        printPersons(persons, new Checker() {
            @Override public boolean test(Person p) {
                return p.age >= 30;
            }
        });
    }

    public static void printPersons(
            List<Person> roster, Checker tester) {
        for (Person p : roster) {
            if (tester.test(p)) {
                p.printPerson();
            }
        }
    }



}
