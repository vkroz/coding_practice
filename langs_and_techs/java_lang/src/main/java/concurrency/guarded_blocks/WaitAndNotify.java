package concurrency.guarded_blocks;

/**
 * Created by vkroz on 4/19/16.
 */
public class WaitAndNotify {
    public static class Data {
        Boolean joy = false;

        public synchronized void guardedJoy() {
            System.out.printf("%s : guardedJoy(): enter\n", Thread.currentThread().getName());

            // This guard only loops once for each special event, which may not
            // be the event we're waiting for.
            while(!joy) {
                try {
                    System.out.printf("%s : guardedJoy(): waiting...\n", Thread.currentThread().getName());
                    wait();
                } catch (InterruptedException e) {
                    System.out.printf("%s : guardedJoy(): InterruptedException\n", Thread.currentThread().getName());
                }
            }
            System.out.printf("%s : guardedJoy(): joy is True\n", Thread.currentThread().getName());
            System.out.printf("%s : guardedJoy(): leave\n", Thread.currentThread().getName());
        }

        public synchronized void notifyJoy() {
            System.out.printf("%s : notifyJoy(): enter\n", Thread.currentThread().getName());
            joy = true;
            notifyAll();
            System.out.printf("%s : notifyJoy(): leave\n", Thread.currentThread().getName());
        }
    }


    public static void main(String[] args) {
        // Create two threads.
        // One will increment A
        // Another will increment b
        // since both

        Data data = new Data();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                data.guardedJoy();
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                data.notifyJoy();
            }
        });

        t1.start();
        t2.start();

    }

}
