package concurrency;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Demonstrates how to start/stop worker threads and gracefully shutdown main driver
 * program when all workers are properly stopped
 */
public class ExecutionControlDemo {
    Random rnd = new Random(System.currentTimeMillis());


    public static void main(String[] args) {
        new ExecutionControlDemo().start();
    }

    private void start() {
        int nWorkers = 3;

        ExecutorService executorService = Executors.newFixedThreadPool(nWorkers);
        CountDownLatch doneSignal = new CountDownLatch(nWorkers);
        AtomicBoolean imAlive = new AtomicBoolean(true);
        System.out.println("Driver: start workers");
        for(int i=0; i<nWorkers; i++) {
            executorService.execute(new Worker(imAlive, doneSignal));
        }

        System.out.println("Driver: set shutdown hook");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Driver: caught shutdown hook");
                imAlive.getAndSet(false);

                try {
                    doneSignal.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                executorService.shutdown();
                try {
                    executorService.awaitTermination(4, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("Driver: all workers stopped, exiting with piece");

            }
        });

        System.out.println("Driver: wait for stop command");
        try {
            doneSignal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    static class Worker implements Runnable {
        private final AtomicBoolean imAlive;
        Random rnd = new Random(System.currentTimeMillis());
        private final CountDownLatch doneSignal;
        int count = 0;
        public Worker(AtomicBoolean imAlive, CountDownLatch latch) {
            this.doneSignal = latch;
            this.imAlive = imAlive;
        }

        @Override
        public void run() {
            System.out.println("Worker: start working...");
            while(imAlive.get()) {
                doSomething();
            }
            System.out.println("Worker: ack driver");
            doneSignal.countDown();
            System.out.println("Worker: exited");
        }

        private void doSomething() {
            try {
                System.out.println("Worker: " + Thread.currentThread().getId() + " count=" + count++);
                Thread.sleep(rnd.nextInt(4*1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
