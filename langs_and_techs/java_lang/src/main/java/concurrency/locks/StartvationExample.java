package concurrency.locks;

import java.util.Random;

/**
 * Purpose: Starvation example
 *
 * <pre>
 *     Starvation describes a situation where a thread is unable to gain regular access to shared resources and is
 * unable to make progress. This happens when shared resources are made unavailable for long periods by "greedy"
 * threads. For example, suppose an object provides a synchronized method that often takes a long time to return. If one
 * thread invokes this method frequently, other threads that also need frequent synchronized access to the same object
 * will often be blocked.
 * </pre>
 *
 * One of the threads will almost never have a chance to run, because other thread runs continuosly and
 * there is no time for unlock object
 *
 * How to fix:
 * option #1: add random delay after call to shared object, so another thread will have a chance to aquire access
 * option #2: remove redundant synchronisation, since 2 threads are not changing same attributed inside shared object
 */
public class StartvationExample {

    public static class Data {
        int a;
        int b;

        Random rnd = new Random(System.currentTimeMillis());

        synchronized void incA() {
            System.out.printf("%s : enter incA()\n", Thread.currentThread().getName());
            a++;
            try {
                Thread.sleep(rnd.nextInt(5) * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf("%s : leave incA()\n", Thread.currentThread().getName());

        }

        synchronized void incB() {
            System.out.printf("%s : enter incB()\n", Thread.currentThread().getName());
            b++;
            try {
                Thread.sleep(rnd.nextInt(5) * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf("%s : leave incB()\n", Thread.currentThread().getName());
        }
    }


    public static void main(String[] args) {
        // Create two threads.
        // One will increment A
        // Another will increment b
        // since both

        Data data = new Data();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                Random rnd = new Random(System.currentTimeMillis());
                while (true) {
                    data.incA();
                }


            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                Random rnd = new Random(System.currentTimeMillis());
                while (true) {
                    data.incB();
//                    try {
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                }


            }
        });

        t1.start();
        t2.start();

    }

}
