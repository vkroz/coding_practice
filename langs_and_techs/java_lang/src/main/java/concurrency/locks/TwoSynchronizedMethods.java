package concurrency.locks;

import java.util.Random;

/**
 * Purpose: check statement
 * <pre>
 *     When a thread invokes a synchronized method, it automatically acquires the
 *     intrinsic lock for that method's object and releases it when the method returns.
 *     The lock release occurs even if the return was caused by an uncaught exception.
 * </pre>
 */
public class TwoSynchronizedMethods {

    public static class Data {
        int a;
        int b;

        Random rnd = new Random(System.currentTimeMillis());

        synchronized void incA() {
            System.out.printf("%s : enter incA()\n", Thread.currentThread().getName());
            a++;
            try {
                Thread.sleep(rnd.nextInt(5)*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf("%s : leave incA()\n", Thread.currentThread().getName());

        }

        void incB() {
            System.out.printf("%s : enter incB()\n", Thread.currentThread().getName());
            b++;
            try {
                Thread.sleep(rnd.nextInt(5)*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf("%s : leave incB()\n", Thread.currentThread().getName());
        }
    }


    public static void main(String[] args) {
        // Create two threads.
        // One will increment A
        // Another will increment b
        // since both

        Data data = new Data();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                Random rnd = new Random(System.currentTimeMillis());
                while(true) {
                    data.incA();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }


            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                Random rnd = new Random(System.currentTimeMillis());
                while(true) {
                    data.incB();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }


            }
        });

        t1.start();
        t2.start();

    }

}
