package util;

import org.junit.Test;

import java.util.Collections;
import java.util.PriorityQueue;

/**
 * Implementation note: this implementation provides O(log(n)) time for the enqueuing and dequeuing
 * methods (offer, poll, remove() and add); linear time for the remove(Object) and contains(Object)
 * methods; and constant time for the retrieval methods (peek, element, and size).
 */
public class PriorityQueueDemo {

    @Test public void priorityQueueDemo() {
        PriorityQueue<Integer> pqueue = new PriorityQueue<>();
        pqueue.add(1);
        pqueue.add(5);
        pqueue.add(4);
        pqueue.add(2);
        pqueue.add(3);

        // Iterator returns elements unordered
        for (Integer elem : pqueue) {
            System.out.println(elem);
        }

        System.out.println("--- offer, poll, remove() and add() are O(log(n)) --- ");
        while (pqueue.size() > 0) {
            Integer elem = pqueue.remove();
            System.out.println(elem);
        }
    }
}
