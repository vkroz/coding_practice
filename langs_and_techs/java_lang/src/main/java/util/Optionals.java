package util;

import streams.model.Value;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;
import java.util.Random;

/**
 * Examples with Optional<>
 */
public class Optionals {
    private static final Logger logger = LogManager.getLogger(Optionals.class);

    Random random = new Random();

    public static void main(String[] args) {
        logger.traceEntry();
        Optionals app = new Optionals();
        app.run();
        logger.traceExit();
    }

    private void run() {
        Optional<Value> value = getValue();
        boolean b = value.isPresent();
        Value v = value.get();

        value = getNullabaleValue();
        b = value.isPresent();
        v = value.get();

        value = getNullValue();
        b = value.isPresent();
        v = value.orElse(Value.EMPTY);

        int i = value.map(Value::getValue).orElse(0);
        System.out.println(i);


    }

    Optional<Value> getValue() {
        Value value = new Value(random.nextInt());
        return Optional.of(value);
    }

    Optional<Value> getNullabaleValue() {
        Value value = new Value(random.nextInt());
        return Optional.ofNullable(value);
    }

    Optional<Value> getNullValue() {
        return Optional.ofNullable(null);
    }
}


