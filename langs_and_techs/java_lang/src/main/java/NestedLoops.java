import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.String.format;

/**
 * Created by vkroz on 11/30/16.
 */
public class NestedLoops {
    public static void main(String[] args) {
        final Set<List<String>> whiteList;
        whiteList = new HashSet<>();
        whiteList.add( Arrays.asList("&a=SEARCH_VIEW", "&ctx=SearchResults&") );
        whiteList.add( Arrays.asList("&a=BROWSE_VIEW", "&ctx=Browse&") );

        System.out.println("----");
        int i=0;
        for(List<String> tuple: whiteList) {
            i++;
            int j = 0;
            for(String item: tuple) {
                j++;
                System.out.println(format("%d.%d: %s", i, j, item));
            }
        }
        System.out.println("----");

        System.out.println("-- break interrupts only nested loop, but outer loop continues --");
        i=0;
        for(List<String> tuple: whiteList) {
            i++;
            int j = 0;
            for(String item: tuple) {
                j++;
                System.out.println(format("%d.%d: %s", i, j, item));
                if(item.equals("&a=SEARCH_VIEW"))
                    break;
            }
        }
        System.out.println("----");

        System.out.println("-- Here to break all loops we use a condition variable --");
        boolean proceed = true;
        i=0;
        for(List<String> tuple: whiteList) {
            i++;
            int j = 0;
            for(String item: tuple) {
                j++;
                System.out.println(format("%d.%d: %s", i, j, item));
                if(item.equals("&a=SEARCH_VIEW")) {
                    proceed = false;
                    break;
                }
            }
            if( !proceed ) {
                break;
            }
        }
        System.out.println("----");

    }
}
