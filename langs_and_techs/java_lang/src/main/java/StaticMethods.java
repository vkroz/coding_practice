public class StaticMethods {
    public static void main(String[] args) {
        System.out.println("---------");
        StaticMethods app = new StaticMethods();
        app.run();
        System.out.println("---------");
    }

    private void run() {
        B a = new B();
        System.out.println( a.calc(2,3) );
    }

    static class A {
        public static int calc(int a, int b) {
            return a+b;
        }
    }

    static class B extends A {
        public static int calc(int a, int b) {
            return a-b;
        }
    }
}
