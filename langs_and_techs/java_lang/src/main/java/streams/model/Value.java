package streams.model;

import com.google.common.base.MoreObjects;

public class Value {
    public static final Value EMPTY = new Value(0);
    int value;

    public Value(int value) {
        this.value = value;
    }

    @Override public String toString() {
        return MoreObjects.toStringHelper(this).add("value", value).toString();
    }

    public int getValue() {
        return value;
    }
}
