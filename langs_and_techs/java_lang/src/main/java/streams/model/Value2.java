package streams.model;

import com.google.common.base.MoreObjects;

public class Value2 {
    public static final Value2 EMPTY = new Value2(0);
    int value;

    public Value2(int value) {
        this.value = value;
    }

    @Override public String toString() {
        return MoreObjects.toStringHelper(this).add("value", value).toString();
    }

    public int getValue() {
        return value;
    }
}
