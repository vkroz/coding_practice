/**
 * Created by vkroz on 1/14/17.
 */
public class CatchNPE {
    public static void main(String[] args) {
        System.out.println("---------");
        CatchNPE app = new CatchNPE();
        app.run();
        System.out.println("---------");
    }

    private void run() {
        try {
            System.out.println("1");
            String a = null;
            System.out.println("2");
            String b = a.toLowerCase();
            System.out.println("3");
            System.out.println("b=" + b);

        } catch (NullPointerException e) {
            System.out.println("Caught NPE");
            e.printStackTrace();
        } finally {
            System.out.println("And handled gracefully");
        }
    }
}
