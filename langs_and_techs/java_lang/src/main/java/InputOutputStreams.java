import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Consumer;

public class InputOutputStreams {

    /**
     * Java8 way to read text file using Stream API
     */
    @Test public void readFromTextFile()  {
        URL path = this.getClass().getResource("/kafka_payload_samples/data_sample.txt");

        try {
            final StringBuilder sb = new StringBuilder();
            Files.lines(Paths.get(path.toURI())).forEach(new Consumer<String>() {
                @Override public void accept(String s) {
                    System.out.println(s);
                    sb.append(s);
                }
            });
            System.out.println("-----------------");
            System.out.println(sb.length());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
