import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

public class ResourcePaths {

    /**
     * Get resource path by name via classloader
     * Note: path must be relative here
     */
    @Test public void getResourcePath() {
        ClassLoader loader = this.getClass().getClassLoader();

        // in some function
        URL path = loader.getResource("kafka_payload_samples/data_sample.txt");
        try {
            File file = new File(path.toURI());
            Assert.assertTrue(file.exists());
        } catch (URISyntaxException e) {
            Assert.fail(e.getMessage());
        }
    }


    /**
     * Get resource path by name without classloader
     * Note: path must be absolute here
     */
    @Test public void getResourcePath2() {

        URL path = this.getClass().getResource("/kafka_payload_samples/data_sample.txt");
        try {
            File file = new File(path.toURI());
            Assert.assertTrue(file.exists());
        } catch (URISyntaxException e) {
            Assert.fail(e.getMessage());
        }
    }

}
