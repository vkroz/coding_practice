
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class Factorial {

    public static int factorial(int n) {
        if( n == 0 )
            return 1;
        else
            return n * factorial(n-1);
    }

    @Test
    public void testFactorial() {
        assertThat(factorial(0), is(1));
        assertThat(factorial(1), is(1));
        assertThat(factorial(2), is(2));
        assertThat(factorial(3), is(6));
        assertThat(factorial(4), is(24));
    }

}
