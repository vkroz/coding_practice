
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class Euler {

    /**
     * e = (n + 1/n)^n
     * @return e
     */
    public static double e1(int precision) {
        return Math.pow(precision + 1.0d/precision, precision);
    }

    /**
     * e = 1/0! + 1/1! + 1/2! + 1/3! + ... + 1/n!
     * @return e
     */
    public static double e2(int precision) {
        double e = 0;
        for(int n=0; n <= precision; n++) {
            e += 1.0d/Factorial.factorial(n);
        }
        return e;
    }

    @Test
    public void testE1() {
        double e = e1(1);
        assertThat(e, is(2.0));
    }

    @Test
    public void testE2() {
        double e = e2(1);
        assertThat(e, is(2.0));
        e = e2(2);
        assertThat(e, is(2.5));
        e = e2(3);
        assertThat(e, is(2.6666666666666665));
        e = e2(4);
        assertThat(e, is(2.708333333333333));
        e = e2(10);
        assertThat(e, is(2.7182818011463845));
    }

}
