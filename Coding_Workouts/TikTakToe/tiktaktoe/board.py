ROWS = 3
COLS = 3
EMPTY_CELL = ' '


class Move(object):
    def __init__(self, char, pos):
        self.char = char
        self.pos = pos

    def __str__(self):
        return "{}: '{}'".format(self.pos, self.char)


class Board():
    def __init__(self):
        self._state = [' '] * ROWS * COLS

    def _idx(self, row, col):
        return row * COLS + col

    def get_cell(self, row, col):
        return self._state[self._idx(row, col)]

    def apply_move(self, move):
        self._state[move.pos] = move.char

    def get_free_moves(self):
        ret = [x for x in range(0, len(self._state)) if self._state[x] == EMPTY_CELL]
        return ret

    def get_lines(self):
        l1 = self._get_rows()
        l2 = self._get_cols()
        l3 = self._get_diags()
        return l1 + l2 + l3

    def _get_rows(self):
        result = []
        for r in range(0, ROWS):
            row = []
            for c in range(0, COLS):
                row.append(self._state[self._idx(r, c)])
            result.append(row)
        return result

    def _get_cols(self):
        result = []
        for c in range(0, COLS):
            col = []
            for r in range(0, ROWS):
                col.append(self._state[self._idx(r, c)])
            result.append(col)
        return result

    def _get_diags(self):
        result = []
        diag1 = []
        diag2 = []
        for idx in range(0, max(ROWS, COLS)):
            diag1.append(self._state[self._idx(idx, idx)])
            diag2.append(self._state[self._idx(ROWS - idx - 1, idx)])
        result.append(diag1)
        result.append(diag2)
        return result

    def __str__(self):
        """
        rowN *
        0 1 2   0,0  0,1  0,2
        3 4 5   1,0  1,1  1,2
        6 7 8   2,0  2,1  2,2
        :return:
        """
        str_buf = ''
        for row in range(0, ROWS):

            for col in range(0, COLS):
                str_buf += '+---'
            str_buf += '+\n'

            for col in range(0, COLS):
                idx = row * COLS + col
                str_buf += '| ' + str(self.get_cell(row, col)) + ' '
            str_buf += '|\n'

        for col in range(0, COLS):
            str_buf += '+---'
        str_buf += '+\n'

        return str_buf


if __name__ == '__main__':
    board = Board()
    board._state = '?' * ROWS * COLS
    print(board)
