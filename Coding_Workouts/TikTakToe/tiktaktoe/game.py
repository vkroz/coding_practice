import copy
import logging

from tiktaktoe.board import *
from tiktaktoe.players import SimplePlayer

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def is_all_equal(lst):
    """
    Returns true if all elements of list are equal
    """
    return lst.count(lst[0]) == len(lst) and lst[0] != ' '



class Game(object):
    def __init__(self, player1, player2, board_state=Board()):
        self.player1 = player1
        self.player2 = player2
        self.current_player = None
        self._state = board_state
        self._moves = []
        self._state_history = []

    def play(self):
        print(self._state)

        while not game.is_over():
            self.current_player = self.next_player()
            move = self.make_move()

            print("Player: {}, Move: {}".format(self.current_player, move))
            print(self._state)

        self._winner = self.current_player

    def next_player(self):
        return self.player2 if self.current_player == self.player1 else self.player1

    def make_move(self):
        """
        Makes next move and updates history
        If no moves left, then sets 'game over' attribute and determines winner
        :return: void
        """
        self._state_history.append(copy.deepcopy(self._state))
        move = self.current_player.make_move(self._state)
        if move is not None:
            self._state.apply_move(move)

        return move

    def is_over(self):
        if len(self._state.get_free_moves()) == 0:
            return True
        else:
            # check any strait line
            for line in self._state.get_lines():
                if is_all_equal(line):
                    return True
            return False

    def get_winner(self):
        self._winner


if __name__ == '__main__':
    player1 = SimplePlayer('x')
    player2 = SimplePlayer('o')
    game = Game(player1, player2)
    game.play()
    print(game.get_winner())
