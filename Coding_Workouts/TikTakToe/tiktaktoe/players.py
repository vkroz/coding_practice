import random

from tiktaktoe.board import Move


class BasePlayer(object):
    def __init__(self, char):
        self._char = char
        self._name = 'base'

    def __str__(self):
        return "{}('{}')".format(self._name, self._char)


class SimplePlayer(BasePlayer):
    def __init__(self, char):
        self._char = char
        self._name = 'SimplePlayer'

    # def __str__(self):
    #     return "{}('{}')".format(self._name, self._char)

    def make_move(self, board):
        """
        :param board:
        :return: Move object, if there is a move available, None - if no moves left
        """
        moves = board.get_free_moves()
        if len(moves) > 0:
            idx = random.randrange(0, len(moves))
            return Move(self._char, moves[idx])
        else:
            return None
