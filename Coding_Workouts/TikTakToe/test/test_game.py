import unittest
from tiktaktoe.game import *

class TestBoard(unittest.TestCase):

    def test_get_cell(self):
        game = Game()
        board.state = [1,2,3, 4,5,6, 7,8,9]

        self.assertEqual(board.get_cell(0,0), 1)
        self.assertEqual(board.get_cell(2,2), 9)
        self.assertEqual(board.get_cell(1,1), 5)


    def test_print_board(self):
        board = Board()
        board._state = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        print(board)
        # self.assertTrue('FOO'.isupper())
        # self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    unittest.main()
