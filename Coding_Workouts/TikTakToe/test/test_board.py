import unittest

from tiktaktoe.board import *


class TestBoard(unittest.TestCase):
    def test_get_cell(self):
        board = Board()
        board._state = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        self.assertEqual(board.get_cell(0, 0), 1)
        self.assertEqual(board.get_cell(2, 2), 9)
        self.assertEqual(board.get_cell(1, 1), 5)

    def test_get_rows(self):
        board = Board()
        board._state = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        rows = board._get_rows()

        self.assertEqual(3, len(rows))
        self.assertEqual([1, 2, 3], rows[0])
        self.assertEqual([4, 5, 6], rows[1])
        self.assertEqual([7, 8, 9], rows[2])

    def test_get_cols(self):
        board = Board()
        board._state = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        cols = board._get_cols()

        self.assertEqual(3, len(cols))
        self.assertEqual([1, 4, 7], cols[0])
        self.assertEqual([2, 5, 8], cols[1])
        self.assertEqual([3, 6, 9], cols[2])

    def test_get_diags(self):
        board = Board()
        board._state = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        diags = board._get_diags()

        self.assertEqual(2, len(diags))
        self.assertEqual([1, 5, 9], diags[0])
        self.assertEqual([7, 5, 3], diags[1])

    def test_lines(self):
        board = Board()
        board._state = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        lines = board.get_lines()

        self.assertEqual(8, len(lines))
        self.assertEqual([1, 2, 3], lines[0])
        self.assertEqual([4, 5, 6], lines[1])
        self.assertEqual([7, 8, 9], lines[2])
        self.assertEqual([1, 4, 7], lines[3])
        self.assertEqual([2, 5, 8], lines[4])
        self.assertEqual([3, 6, 9], lines[5])
        self.assertEqual([1, 5, 9], lines[6])
        self.assertEqual([7, 5, 3], lines[7])

    def test_print_board(self):
        board = Board()
        board._state = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        dump = board.__str__()
        self.assertEqual('+---+---+---+\n' +
                         '| 1 | 2 | 3 |\n' +
                         '+---+---+---+\n' +
                         '| 4 | 5 | 6 |\n' +
                         '+---+---+---+\n' +
                         '| 7 | 8 | 9 |\n' +
                         '+---+---+---+\n', dump)


if __name__ == '__main__':
    unittest.main()
