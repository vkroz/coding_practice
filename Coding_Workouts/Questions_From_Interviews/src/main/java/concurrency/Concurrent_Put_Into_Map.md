Given a simple hashtable implementation
with method

    void put(K k, V v, Customer c)

where Customer is some object representing customer

Implement this `put` method in such a fashion, that when
it is called in multuple threads concurrently, then
threads providing same customer in argument call this method
concurrently, while threads calling this methods with other
customer will wait.

E.g. if at the same time several threads call the method,
then depending on Customer argument the result should be like
this:

Thread 1 : Customer C1 - can complete put
Thread 2 : Customer C2 - will block until T1 and T3 complete
Thread 3 : Customer C1 - can complete put
Thread 4 : Customer C3 - will block until T1 and T3 complete


