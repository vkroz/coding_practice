package mini_challenges;

import org.junit.Test;

import java.util.*;

/**
 * Deep copy a list of nodes (a graph)
 */
public class Graph_Serialize_Deserialize {

    class Person implements Cloneable {
        String name;
        Collection<Person> friends;

        public Person(String name) {
            this.name = name;
        }

        @Override
        protected Object clone() {
            Person newPerson = new Person(this.name);
            newPerson.friends = this.friends;
            return newPerson;
        }


        Person serialize() {
            Person newPerson = (Person) this.clone();

            Map<Person, Person> map = new HashMap<>();
            Queue<Person> queue = new LinkedList<>();
            queue.add(this);
            while(queue.size() > 0) {
                Person p = queue.poll();
                p.clone();
                for(Person f: p.friends) {
                    if( !map.containsKey(f) ) {
                        queue.add(f);
                    }
                }
            }


            return newPerson;
        }
    }

    @Test
    public void testDeepCopy() {

        Person a = new Person("A");
        Person a_cloned = a.serialize();

        System.out.println(this.hashCode());
        System.out.println(a_cloned.hashCode());

    }
}