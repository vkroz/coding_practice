package mini_challenges;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * For a given string and dictionary, how many sentences can you make from the string, such that
 * all the words are contained in the dictionary.
 * <p>
 * eg: for given string -> "appletablet"
 * "apple", "tablet"
 * "applet", "able", "t"
 * "apple", "table", "t"
 * "app", "let", "able", "t"
 * <p>
 * "applet", {app, let, apple, t, applet} => 3
 * "thing", {"thing"} -> 1
 */
public class Make_Sentences {
    int count(String input, Set<String> words) {
        /*
        until dictiionary is  not empty or
            try to find matching substring
            if found
                input = input without matching word
                remove



         */
        for(String word: words) {
            if( input.indexOf(word) > 0 ) {

            }
        }
        return 0;
    }

    @Test
    public void testCount() {
        Make_Sentences app = new Make_Sentences();

        app.count("applet", new HashSet<>(Arrays.asList(new String[] {"app", "let", "apple", "t", "applet"})));
        app.count("applet", new HashSet<>(Arrays.asList(new String[] {"app", "let", "apple", "t", "applet"})));



    }
}
