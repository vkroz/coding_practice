package mini_challenges;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Stack;

/**
 * Phone screen:
 * <pre>
 * 1. Given a stack (don't worry about the actual implementation of the stack),
 * need to make sure that we keep track of max element in the stack... i.e. whenever the stack is popped,
 * the max can change, so getMax() should  always return the max. similarly when a new element is added, the max can change.
 * </pre>
 *
 * Transactions_Balance.Solution
 * <pre>
 *     Maintain sorted list of objects contained in the stack, so that at any moment of time we can have maximum element
 *     Whenever object is removed from stack, it should be removed from priority queue as well
 * </pre>
 */

public class Track_Max_Element_In_Stack {

    @Test
    public void testMyStack() {
        TrackingStack stack = new TrackingStack();
        int nums[] = { 1, 3, 20, 15, 1, 30 };

        stack.push(1);
        Assert.assertEquals(1, (int)stack.getMax());

        stack.push(3);
        Assert.assertEquals(3, (int)stack.getMax());

        stack.push(10);
        Assert.assertEquals(10, (int)stack.getMax());

        stack.push(3);
        Assert.assertEquals(10, (int)stack.getMax());

        stack.push(30);
        Assert.assertEquals(30, (int)stack.getMax());

        Assert.assertEquals(30, (int)stack.pop());
        Assert.assertEquals(10, (int)stack.getMax());

        Assert.assertEquals(3, (int)stack.pop());
        Assert.assertEquals(10, (int)stack.getMax());

        Assert.assertEquals(10, (int)stack.pop());
        Assert.assertEquals(3, (int)stack.getMax());

        Assert.assertEquals(3, (int)stack.pop());
        Assert.assertEquals(1, (int)stack.getMax());

        Assert.assertEquals(1, (int)stack.pop());
        Assert.assertEquals(null, stack.getMax());
    }
}

class TrackingStack extends Stack<Integer> {

    PriorityQueue<Integer> sorted = new PriorityQueue<>(new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            Integer i1 = (Integer) o1;
            Integer i2 = (Integer) o2;
            if (i1 < i2) return 1;
            else if (i1 > i2) return -1;
            else return 0;
        }
    });

    @Override
    public Integer push(Integer item) {
        sorted.add(item);
        return super.push(item);
    }

    @Override
    public synchronized Integer pop() {
        Integer n = super.pop();
        sorted.remove(n);
        return n;
    }

    public Integer getMax() {
        return sorted.peek();
    }

}
