package mini_challenges;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Implement queue using 2 stacks of fixed size
 *
 * @param <E>
 */
public class Queue_From_Stacks<E> implements Queue<E> {

    final int STACK_SIZE = 10;

    Deque<E> inStack; /* use only offerLast, pollLast */
    Deque<E> outStack;

    public Queue_From_Stacks() {
        inStack = new ArrayDeque<E>(STACK_SIZE);
        outStack = new ArrayDeque<E>(STACK_SIZE);
    }

    @Override
    public boolean add(E e) {
        if (inStack.size() == STACK_SIZE) {
            relocate();
        }
        return inStack.offerLast(e);
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    private void relocate() {

    }

    @Override
    public E remove() {
        if (outStack.size() > 0) {
            return outStack.remove();
        } else {
            return inStack.remove();
        }
    }


    @Override
    public int size() {
        return inStack.size() + outStack.size();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(E e) {
        return false;
    }

    @Override
    public E poll() {
        return null;
    }

    @Override
    public E element() {
        return null;
    }

    @Override
    public E peek() {
        return null;
    }


    @Test
    public void testStack() throws Exception {
        Deque<Integer> s = new ArrayDeque<Integer>(3);
        s.offerLast(1);
        s.offerLast(2);
        s.offerLast(3);
        s.offerLast(4);
//    assertEquals(false, s.offerLast(5));


        long i = s.pollLast();
        assertEquals(4, i);

        assertEquals(3, (long) s.pollLast());
        assertEquals(2, (long) s.pollLast());
        assertEquals(1, (long) s.pollLast());
        assertNull(s.pollLast());

    }


    @Test
    public void testQueue() throws Exception {
        Queue_From_Stacks<Integer> q = new Queue_From_Stacks();
        q.add(0);
        q.add(1);
        q.add(2);
        q.add(3);
        q.add(4);
        q.add(5);
        q.add(6);
        q.add(7);
        q.add(8);
        q.add(9);

        assertTrue(q.size() == 10);

        assertEquals(0L, (long) q.remove());
        assertEquals(1L, (long) q.remove());
        assertEquals(2L, (long) q.remove());
        assertEquals(3L, (long) q.remove());
        assertEquals(4L, (long) q.remove());
        assertEquals(5L, (long) q.remove());
        assertEquals(6L, (long) q.remove());
        assertEquals(7L, (long) q.remove());
        assertEquals(8L, (long) q.remove());
        assertEquals(9L, (long) q.remove());
        assertNull(q.poll());

    }

}
