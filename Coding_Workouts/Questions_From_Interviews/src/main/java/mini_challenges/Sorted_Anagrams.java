package mini_challenges;

import org.junit.Test;

import java.util.*;

/**
 * Scanner in = new Scanner(System.in);
 * int n = in.nextInt();
 * int[] ar = new int[n];
 * for (int i = 0; i < n; i++) {
 * ar[i] = in.nextInt();
 * }
 * sort(ar);
 */
public class Sorted_Anagrams {
    public static void main(String[] args) throws Exception {
        Sorted_Anagrams app = new Sorted_Anagrams();
        app.anagrams(null);
    }

    public void anagrams(String debugStr) {

        Map<String, Set<String>> anagrams = new HashMap<>();

        String inStr = null;
        if (null != debugStr) {
            inStr = debugStr;
        } else {
            Scanner in = new Scanner(System.in);
            inStr = in.nextLine();
        }

        List<String> words = tokenize(inStr);
        for (String word : words) {
            String key = this.sortChars(word);
            Set<String> anaWords = null;
            if (anagrams.containsKey(key)) {
                anaWords = anagrams.get(key);
            } else {
                anaWords = new HashSet<>();
                anagrams.put(key, anaWords);
            }
            anaWords.add(word);
        }

        Map<String, List<String>> out = new HashMap<>();
        for (Set<String> tmp : anagrams.values()) {
            List<String> anaWords = new ArrayList<>(tmp);
            Collections.sort(anaWords);
            if (anaWords.size() > 1) {
                String key = anaWords.get(0);
                out.put(key, anaWords);
            }

        }

        List<String> keys = new ArrayList<>(out.keySet());
        Collections.sort(keys);
        for (String key : keys) {
            List<String> anaWords = out.get(key);
            for (int i = 0; i < anaWords.size(); i++) {
                String word = anaWords.get(i);
                System.out.printf("%s", word);
                if (i < anaWords.size() - 1)
                    System.out.printf(" ");
                else
                    System.out.printf("\n");
            }
        }

    }

    /**
     * Tokenize, convert to lowercase, renove all non-alphanumerics
     *
     * @param inStr
     * @return
     */
    private List<String> tokenize(String inStr) {
        String s = inStr.replaceAll("[^A-Za-z0-9 ]", "");
        String[] tmp = s.toLowerCase().split(" ");
        List<String> out = new ArrayList<>();
        for (String w : tmp) {
            if (w.length() > 1)
                out.add(w);
        }
        return out;
    }

    String sortChars(String s) {
        char[] chars = s.toCharArray();
        Arrays.sort(chars);
        return String.valueOf(chars);
    }


    @Test
    public void testAnagrams() {
        Sorted_Anagrams app = new Sorted_Anagrams();

        String debug1 = "apt pat 1st st1 bat tab";
        String debug2 = "Amy and Dan, may be on a tour to watch Redskins rout no other than Patriots";
        String debug3 = "Aunt anna $%# had a nana*, eat tuna %#$, dosa, and drank te-a and soda";

        System.out.println(debug3);
        app.anagrams(debug3);
        System.out.println("-------");

        System.out.println(debug1);
        app.anagrams(debug1);
        System.out.println("-------");

        System.out.println(debug2);
        app.anagrams(debug2);
        System.out.println("-------");
    }

}
