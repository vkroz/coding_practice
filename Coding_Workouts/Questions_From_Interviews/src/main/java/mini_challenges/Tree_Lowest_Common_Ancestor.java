package mini_challenges;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Find lowest common ancestor for given 2 nodes
 * <p>
 * Given tree
 * <pre>
 *
 *                 1
 *          +------+-------+
 *          |              |
 *          2              3
 *     +----+---+     +----+---+
 *     |        |              |
 *     4        5              6
 *         +----+---+     +----+---+
 *         |        |              |
 *         7        8              9
 * </pre>
 * <p>
 * <pre>
 *     lca(root, 7, 8) => 5;
 *     lca(root, 7, 6) => 1;
 *     lca(root, 4, 8) => 2;
 * </pre>
 */
class Node {
    int value;
    Node left;
    Node right;
    public Node(int value, Node left, Node right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

}

/**
 * The solution is to search in tree each requested node, and keep track of visited nodes.
 * Then, once both nodes are found, compare sorted lists of visited nodes of both searches and return least common
 * element
 */
public class Tree_Lowest_Common_Ancestor {

    public static int lca(Node root, int n1, int n2) {
        List<Integer> trace1 = new ArrayList<>();
        _findAndTrace(root, n1, trace1);

        List<Integer> trace2 = new ArrayList<>();
        _findAndTrace(root, n2, trace2);

        int lc = trace1.size()-1;
        int rc = trace2.size()-1;
        int lca = -1;
        while( trace1.get(lc) == trace2.get(rc) ) {
            if( lc < 0 || rc < 0 ) {
                break;
            }

            lca = trace1.get(lc);
            lc--;
            rc--;
        }
        return lca;
    }


    static boolean _findAndTrace(Node node, int value, List<Integer> trace) {
        if( node.value == value) {
            trace.add(node.value);
            return true;
        }
        else {
            boolean lookLeft = false, lookRight = false;
            if( node.left != null ) {
                lookLeft = _findAndTrace(node.left, value, trace);
            }
            if( node.right != null ) {
                lookRight = _findAndTrace(node.right, value, trace);
            }

            if( lookLeft || lookRight ) {
                    trace.add(node.value);
            }
            return ( lookLeft || lookRight );
        }
    }

    // ------------------ testing -------------------------

    @Test
    public void test1_Lca() {
        System.out.println("==== test1_Lca====");
        Node seven = new Node(7, null, null);
        Node eight = new Node(8, null, null);
        Node nine = new Node(9, null, null);
        Node four = new Node(4, null, null);
        Node five = new Node(5, seven, eight);
        Node six = new Node(6, null, nine);
        Node two = new Node(2, four, five);
        Node three = new Node(3, null, six);
        Node one = new Node(1, two, three);
        Node testRoot = one;

        Assert.assertEquals(2, lca(testRoot, 4, 5));
        Assert.assertEquals(2, lca(testRoot, 4, 8));
        Assert.assertEquals(5, lca(testRoot, 7, 8));
        Assert.assertEquals(1, lca(testRoot, 7, 6));
        Assert.assertEquals(1, lca(testRoot, 2, 3));
    }
}

