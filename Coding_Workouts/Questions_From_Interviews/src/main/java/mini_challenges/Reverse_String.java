package mini_challenges;

import org.junit.Assert;
import org.junit.Test;

/**
 * Reverse the string
 */
public class Reverse_String {

    public String reverse(String s) {
        char[] chars = s.toCharArray();

        int l = 0;
        int r = chars.length-1;
        while(l < r) {
            swap(chars, l, r);
            l++;
            r--;
        }
        return new String(chars);
    }

    void swap(char[] a, int l, int r) {
        char tmp = a[l];
        a[l] = a[r];
        a[r] = tmp;
    }

    @Test
    public void testReverse() {
        Reverse_String app = new Reverse_String();
        Assert.assertEquals("cba", app.reverse("abc"));
        Assert.assertEquals("dcba", app.reverse("abcd"));


    }
}
