package mini_challenges;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/*
0 1 2 3 4 ..... 3000
1
 */

public class Minimum_Unique_Array_Sum {

    static int getMinimumUniqueSum(int[] arr) {

        Map<Integer, Integer> slots = new HashMap<>();

        for(int i=0; i<arr.length; i++) {
            if( slots.containsKey(arr[i]))
                slots.put(arr[i], slots.get(arr[i]) + 1);
            else
                slots.put(arr[i], 1);
        }

        List<Integer> vals = new ArrayList<>(slots.keySet());
        for(Integer key: vals) {
            while(slots.get(key) > 1) {
                int val = findNextAvailable(slots, key);
                slots.put(val, 1);
                slots.put(key, slots.get(key)-1);
            }
        }

        int sum = slots.keySet().stream().mapToInt(Integer::intValue).sum();
        return sum;
    }

    private static int findNextAvailable(Map<Integer, Integer> slots, Integer startVal) {
        for( int val = startVal; val < 5001; val++) {
            if( !slots.containsKey(val) )
                return val;
        }
        // should not reach here
        return 0;
    }

    @Test public void getMinimumUniqueSumTest() {

        Assert.assertEquals(14,
                Minimum_Unique_Array_Sum.getMinimumUniqueSum(new int[] {2, 2, 4, 5}));
        Assert.assertEquals(33,
                Minimum_Unique_Array_Sum.getMinimumUniqueSum(new int[] {10,10,10}));
        Assert.assertEquals(6,
                Minimum_Unique_Array_Sum.getMinimumUniqueSum(new int[] {1, 2, 2}));
        Assert.assertEquals(6,
                Minimum_Unique_Array_Sum.getMinimumUniqueSum(new int[] {1, 2, 3}));
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        final String fileName = System.getenv("OUTPUT_PATH");
        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        int res;

        int _arr_size = 0;
        _arr_size = Integer.parseInt(in.nextLine().trim());
        int[] _arr = new int[_arr_size];
        int _arr_item;
        for (int _arr_i = 0; _arr_i < _arr_size; _arr_i++) {
            _arr_item = Integer.parseInt(in.nextLine().trim());
            _arr[_arr_i] = _arr_item;
        }

        res = getMinimumUniqueSum(_arr);
        bw.write(String.valueOf(res));
        bw.newLine();

        bw.close();
    }
}


