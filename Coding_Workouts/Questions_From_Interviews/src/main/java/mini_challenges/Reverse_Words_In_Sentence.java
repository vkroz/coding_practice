package mini_challenges;

import org.junit.Assert;
import org.junit.Test;

import java.util.Stack;

/**
 * Reverse words in the string
 */
public class Reverse_Words_In_Sentence {

    public String method1(String input) {
        String[] tokens = input.split("\\s+");
        StringBuilder sb = new StringBuilder();
        for (int i = tokens.length - 1; i >= 0; i--) {
            sb.append(tokens[i]);
            if (i > 0)
                sb.append(" ");
        }
        return sb.toString();
    }

    public String method2(String input) {
        StringBuilder buf = new StringBuilder();
        int beginIndex = input.length() - 1;
        int endIndex = input.length();

        while (beginIndex >= 0) {

            // rewind startPos to start of current word
            while (beginIndex > 0 && input.charAt(beginIndex) != ' ')
                beginIndex--;

            // Select word from string
            String s = input.substring(((beginIndex == 0) ? beginIndex
                    : beginIndex + 1), endIndex);
            buf.append(s);
            if (beginIndex != 0)
                buf.append(" ");

            // Reposition pointers
            endIndex = beginIndex--;
        }

        return buf.toString();
    }

    public String method3(String input) {
        Stack<String> stack = new Stack<String>();

        // Scan string chars
        StringBuilder buf = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if (ch != ' ') {
                buf.append(ch);
            } else {
                // A word has been read - place buffer into stack
                stack.push(buf.toString());
                buf = new StringBuilder();
            }
        }
        stack.push(buf.toString());

        StringBuilder retval = new StringBuilder();
        while (!stack.empty()) {
            retval.append(stack.pop());
            if (stack.size() > 0)
                retval.append(" ");
        }
        return retval.toString();
    }


    @Test
    public void testMyMethods() throws Exception {
        Reverse_Words_In_Sentence app = new Reverse_Words_In_Sentence();


        String rs = app.method2("abc xyz");
        System.out.println(rs);
        Assert.assertEquals("xyz abc", rs);


        rs = app.method2("A Cow Jumped Over The Moon");
        System.out.println(rs);
        Assert.assertEquals("Moon The Over Jumped Cow A", rs);

        rs = app.method1("A Cow Jumped Over The Moon");
        System.out.println(rs);
        Assert.assertEquals("Moon The Over Jumped Cow A", rs);

        rs = app.method3("A Cow Jumped Over The Moon");
        System.out.println(rs);
        Assert.assertEquals("Moon The Over Jumped Cow A", rs);
    }

}
