package mini_challenges;

/**
 * Calculate angle between clock's arrows
 *
 */
public class Angle_Between_Clocks_Arrows {
	public static int angle(int hour, int minute) {
		return Math.abs((hour*30-minute*6));
	}
}
