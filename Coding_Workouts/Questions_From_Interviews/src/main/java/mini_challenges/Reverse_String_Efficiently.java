package mini_challenges;

import org.junit.Assert;
import org.junit.Test;

public class Reverse_String_Efficiently {

    @Test
	public void testReverse() {
        Reverse_String_Efficiently app = new Reverse_String_Efficiently();
        Assert.assertTrue(app.reverse("qwerty").equals("ytrewq"));
    }

    /**
     * Reverses input string
     *
     * @return Reversed string
     */
    public String reverse(String s1) {
        byte[] c = s1.getBytes();
        int ix1 = 0;
        int ix2 = s1.length() - 1;
        while (ix1 < ix2) {
            c[ix1] ^= c[ix2];
            c[ix2] ^= c[ix1];
            c[ix1] ^= c[ix2];
            ix1++;
            ix2--;
        }
        return new String(c);
    }

}
