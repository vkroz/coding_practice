package mini_challenges;
/**
 * This class will be given a list of words (such as might be tokenized from a paragraph of text), and will provide a
 * method that takes two words and returns the shortest distance (in words) between those two words in the provided
 * text. Example:
 * <p>
 * <pre>
 *  WordDistanceFinder finder = new WordDistanceFinder(old.Arrays.asList("the", "quick", "brown", "fox", "quick"));
 *  assert(finder.distance("fox","the") == 3);
 *  assert(finder.distance("quick", "fox") == 1);
 * </pre>
 * <p>
 * "quick" appears twice in the input. There are two possible distance values for "quick" and "fox": (3 - 1) = 2 and (4
 * - 3) = 1. Since we have to return the shortest distance between the two words we return 1.
 * <p>
 * <code>
 * <pre>
 *  public class WordDistanceFinder {
 *      public WordDistanceFinder (List<String> words) {
 *      }
 *
 *      public int distance (String wordOne, String wordTwo) {
 *      }
 *  }
 * </pre>
 * </code>
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class Minimal_Distance_In_Array {

    public Minimal_Distance_In_Array() {
    }

    public int distance(List<String> words, String wordOne, String wordTwo) {

        Map<String, List<Integer>> wordsMap = fillDictionary(words);


        List<Integer> leftWordPos = wordsMap.get(wordOne);
        List<Integer> rightWordPos = wordsMap.get(wordTwo);

        // check is entries not exists
        if (leftWordPos == null || rightWordPos == null)
            return -1;

        Collections.sort(leftWordPos);
        Collections.sort(rightWordPos);

        Integer minDist = null;
        int i = 0;
        int j = 0;
        for (; i < leftWordPos.size() || j < rightWordPos.size(); ) {


            if (i == leftWordPos.size() && j < rightWordPos.size()) {
                minDist = getMin(minDist, Math.abs(leftWordPos.get(i - 1) - rightWordPos.get(j)));
                j++;
            } else if (i < leftWordPos.size() && j == rightWordPos.size()) {
                minDist = getMin(minDist, Math.abs(leftWordPos.get(i) - rightWordPos.get(j - 1)));
                i++;
            } else if (leftWordPos.get(i) < rightWordPos.get(j)) {
                minDist = getMin(minDist, Math.abs(leftWordPos.get(i) - rightWordPos.get(j)));
                i++;
            } else if (leftWordPos.get(i) > rightWordPos.get(j)) {
                minDist = getMin(minDist, Math.abs(leftWordPos.get(i) - rightWordPos.get(j)));
                j++;
            } else {
                return 0;
            }

        }

        return minDist;
    }

    private Integer getMin(Integer min, Integer n) {
        if (min == null)
            return new Integer(n);
        else
            return Math.min(min, n);
    }

    private Map<String, List<Integer>> fillDictionary(List<String> words) {
        Map<String, List<Integer>> wordsMap = new HashMap<>();

        for (int i = 0; i < words.size(); i++) {
            String word = words.get(i);
            List<Integer> positions = wordsMap.get(word);
            if (positions == null) {
                positions = new ArrayList<>();
                wordsMap.put(word, positions);
            }

            positions.add(i);
        }

        return wordsMap;
    }

    @Test
    public void testDistance() {
        Minimal_Distance_In_Array finder = new Minimal_Distance_In_Array();

        List<String> words = Arrays.asList("the", "quick", "brown", "fox", "quick");
        Assert.assertEquals(3, finder.distance(words, "fox", "the"));
        Assert.assertEquals(1, finder.distance(words, "quick", "fox"));

    }
}
