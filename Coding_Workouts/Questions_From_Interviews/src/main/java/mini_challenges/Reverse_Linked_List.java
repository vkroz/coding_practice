package mini_challenges;

import org.junit.Test;

/**
 * Reverse linked list
 */
public class Reverse_Linked_List {
    static class Node {
        public Node(int data) {
            this.data = data;
        }
        Node next;
        int data;

        public void printList() {
            for( Node n = this; n != null; n = n.next) {
                System.out.printf("%d", n.data);
            }
            System.out.println("");

        }


        public Node reverse() {

            Node prev = null;
            Node curr = this;
            Node next = curr.next;

            while(next != null ){
                curr.next = prev;
                prev = curr;
                curr = next;
                next = next.next;
            };
            curr.next = prev;
            return curr;
        }
    }


    @Test
    public void testReverse() {
        System.out.println("--- testing reverse of linked list ---");
        Node head = new Node(0);
        Node n = head;
        for(int i=1;  i < 10; i++) {
            n.next = new Node(i);
            n = n.next;
        }
        Node tail = n;

        head.printList();
        Node rhead = head.reverse();
        rhead.printList();
    }
}
