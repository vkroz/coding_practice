package mini_challenges;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * ----------------------------------------------------
 * <pre>
 *      Find maximum slice in array
 *      [1,2,3,4,5] must give [1,2,3,4,5]
 *      [99,-10,99,1] must give [99,1]
 *      [5, 5, 5, -5, 5, 5] must give [5, 5, 5, -5, 5, 5]
 *      [5, 5, 5, -55, 5, 5] must give [5, 5, 5]
 * </pre>
 * <p>
 * Transactions_Balance.Solution
 * <pre>
 *    for each index
 *      compute sum (from index to
 * </pre>
 */
public class Max_Slice_Of_Array {

    public Integer[] solution_O3(Integer[] ar) {
        int max_sum = Integer.MIN_VALUE;
        int max_li = 0, max_ri = 0;
        for (int li = 0; li < ar.length; li++) {
            for (int ri = li; ri < ar.length; ri++) {
                int sum = 0;
                for (int i = li; i <= ri; i++) {
                    sum += ar[i];
                }
                if (sum > max_sum) {
                    max_sum = sum;
                    max_li = li;
                    max_ri = ri;
                    printSlice(ar, max_li, max_ri, sum);
                }
            }
        }

        List<Integer> retval = new ArrayList<>();
        for (int i = max_li; i <= max_ri; i++) {
            retval.add(ar[i]);
        }
        return retval.toArray(new Integer[retval.size()]);
    }

    private void printSlice(Integer[] ar, int max_li, int max_ri, int sum) {
        System.out.printf("[ ");
        for (int i = max_li; i <= max_ri; i++) {
            System.out.printf("%d", ar[i]);
            if (i < max_ri)
                System.out.printf(", ");
            else
                System.out.printf(" ] => %d\n", sum);
        }
    }

    private String arrayToString(Integer[] ar) {
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        for (int i = 0; i < ar.length; i++) {
            sb.append(String.format("%d", ar[i]));
            if (i < ar.length-1)
                sb.append(String.format(", "));
            else
                sb.append(String.format(" ]"));
        }
        return sb.toString();
    }

    @Test
    public void testSolution() {
        Max_Slice_Of_Array app = new Max_Slice_Of_Array();

        Integer[] ar1 = new Integer[]{1, 2, 3, 4, 5}; //must give [1,2,3,4,5]
        System.out.printf("Input array: %s\n", app.arrayToString(ar1));
        Integer[] res = app.solution_O3(ar1);
        System.out.printf("Max slice: %s\n\n", app.arrayToString(res));

        Integer[] ar2 = new Integer[]{99, -10, 5, 1}; // must give [99,1]
        System.out.printf("Input array: %s\n", app.arrayToString(ar2));
        res = app.solution_O3(ar2);
        System.out.printf("Max slice: %s\n\n", app.arrayToString(res));

        Integer[] ar3 = new Integer[]{5, 5, 5, -25, 25, 5}; // must give [5, 5, 5, -5, 5, 5]
        System.out.printf("Input array: %s\n", app.arrayToString(ar3));
        res = app.solution_O3(ar3);
        System.out.printf("Max slice: %s\n\n", app.arrayToString(res));

        Integer[] ar4 = new Integer[]{5, 5, 5, -55, 5, 5}; // must give [5, 5, 5]
        System.out.printf("Input array: %s\n", app.arrayToString(ar4));
        res = app.solution_O3(ar4);
        System.out.printf("Max slice: %s\n\n", app.arrayToString(res));


    }
}
