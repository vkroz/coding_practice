package mini_challenges;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Read in N Strings and determine if they are anagrams of each other. Ex) 'cat','act','tac' -> true 'cat', 'bat', 'act'
 * -> false
 * <p>
 * 2. Read in strings and sort them.
 */
public class Count_Anagrams {
    public static boolean isAnagram(String s1, String s2) {
        if (s1.length() != s2.length())
            return false;

        Map<Character, Integer> chars = new HashMap<>();
        for (char c : s1.toCharArray()) {
            Integer cnt = chars.get(c);
            if (cnt != null) {
                chars.put(c, cnt++);
            } else {
                chars.put(c, 1);
            }
        }

        for (char c : s2.toCharArray()) {
            Integer cnt = chars.get(c);
            if (cnt != null) {
                if (cnt > 1)
                    chars.put(c, cnt--);
                else
                    chars.remove(c);
            } else {
                return false;
            }
        }

        return (chars.size() == 0) ? true : false;

    }


    public static void main(String[] args) {

        String[] words = {"qwerty", "ytrewq", "qwreyt", "yqtwre", "yqtwreqwert", "table",
                "bleat", "abelt", "batel", "tabel", "labet"};
        List<List<String>> anagrams = new ArrayList<>();

        for (String word : words) {
            boolean found = false;
            for (List<String> alist : anagrams) {
                String test = alist.get(0);
                if (test != null) {
                    if (Count_Anagrams.isAnagram(word, test)) {
                        alist.add(word);
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {
                List<String> l = new ArrayList<>();
                l.add(word);
                anagrams.add(l);
            }
        }

        for (List<String> a : anagrams) {
            System.out.println(a);
        }
        System.out.println("--- done ---");
    }
}
