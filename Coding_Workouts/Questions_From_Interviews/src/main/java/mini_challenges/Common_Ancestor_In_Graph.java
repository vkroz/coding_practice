package mini_challenges;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/*
 1   2   4
  \ /   / \
   3   5   8
    \ / \   \
     6   7   9


Write a function that takes this data as input and returns two collections: one containing all individuals with zero known parents, and one containing all individuals with exactly one known parent.
Sample output (pseudocode):
    [
      [1, 2, 4], //Individuals with zero parents
      [5, 7, 8, 9] //Individuals with one parent
    ]

Write a function that, for two given individuals in our dataset, returns true if and only if they share at least one ancestor.
Sample input and output:
    [3, 8] => false
    [5, 8] => true
    [6, 8] => true

 */
public class Common_Ancestor_In_Graph {

    @Test public void testHasAncestor() {
        int[][] parentChildPairs =
                new int[][] {{1, 3}, {2, 3}, {3, 6}, {5, 6}, {5, 7}, {4, 5}, {4, 8}, {8, 9}};


        Assert.assertTrue(hasAncestor(parentChildPairs, 6, 8));
        Assert.assertFalse(hasAncestor(parentChildPairs, 3, 8));
        Assert.assertTrue(hasAncestor(parentChildPairs, 5, 8));
    }

    public static boolean hasAncestor(int[][] data, int a, int b) {
        Map<Integer, List<Integer>> adjList = new HashMap<>();
        for(int[] pair: data) {
            int parent = pair[0];
            int child = pair[1];
            if( !adjList.containsKey(child)) {
                adjList.put(child, new ArrayList<>(Arrays.asList(parent)));
            }
            else {
                adjList.get(child).add(parent);
            }
        }

        Set<Integer> parentsA = getParents(Integer.valueOf(a), adjList, new HashSet<>());
        Set<Integer> parentsB = getParents(Integer.valueOf(b), adjList, new HashSet<>());


        for(Integer pa: parentsA) {
            if( parentsB.contains(pa))
                return true;
        }
        return false;
    }

    /**
     1   2   4
     \ /   / \
     3   5   8
     \ / \   \
     6   7   9

        reverse adjacency
     * ================
     * 3: 1, 2
     * 6: 3, 5
     * 7: 5
     * 5: 4
     * 8: 4
     * 9: 8
     */
    private static Set<Integer> getParents(Integer child, Map<Integer, List<Integer>> adjList,
            Set<Integer> parents) {
        if(!adjList.containsKey(child)) {
            return parents;
        }
        else {
            for(Integer parent: adjList.get(child)) {
                parents.add(parent);
                getParents(parent, adjList, parents);
            }
        }
        return parents;
    }

}
