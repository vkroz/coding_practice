package mini_challenges;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * Using k sorted lists of integers, create single sorted list from these lists
 * E.g. given
 * [1,2,3,5,7]
 * [2,3,4]
 * [5,6,7,8,9]
 * should result in
 * [1,2,2,3,3,4,5,5,6,7,8,9]
 */

public class N_Way_Merge {
    List<Integer> merge(List<Integer>... lists) {

        List<Integer> retval = new ArrayList<>();
        int[] ptrs = new int[lists.length];

        PriorityQueue<Value> nWayCursor = new PriorityQueue<>(new Comparator<Value>() {
            @Override
            public int compare(Value o1, Value o2) {
                return Integer.compare(o1.value, o2.value);
            }
        });

        // initialize
        for (int k = 0; k < lists.length; k++) {
            nWayCursor.add(new Value(k, lists[k].get(0)));
        }

        // iterate until all pointers will reach end of respective lists
        while (nWayCursor.size() > 0) {

            // getting next least value
            Value min = nWayCursor.poll();
            int listN = min.listN;
            int value = min.value;
            // and write into result
            retval.add(value);

            // advance pointer in the list
            List<Integer> list = lists[listN];
            int itemPtr = ptrs[listN];
            if (itemPtr < list.size()) {
                if (itemPtr < list.size() - 1) {
                    itemPtr++;
                    nWayCursor.add(new Value(listN, list.get(itemPtr)));
                    ptrs[listN] = itemPtr;
                }
            }

        }

        return retval;
    }

    /**
     * A pair: listNumber, value
     */
    class Value {

        public Value(int listNum, int value) {
            this.listN = listNum;
            this.value = value;
        }

        int value;
        int listN;
    }

    @Test
    public void testMerge() {
        N_Way_Merge app = new N_Way_Merge();

        List<Integer> l1 = Arrays.asList(1, 2, 3, 5, 7);
        List<Integer> l2 = Arrays.asList(2, 3, 4);
        List<Integer> l3 = Arrays.asList(5, 6, 7, 8, 9);
        List<Integer> actual = app.merge(l1, l2, l3);
        List<Integer> expected = Arrays.asList(1, 2, 2, 3, 3, 4, 5, 5, 6, 7, 7, 8, 9);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());


        l1 = Arrays.asList(-1);
        l2 = Arrays.asList(200);
        l3 = Arrays.asList(-1010103);
        actual = app.merge(l1, l2, l3);
        expected = Arrays.asList(-1010103, -1, 200);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());

        l1 = Arrays.asList(1,3,4,5,6,7,8,9);
        l2 = Arrays.asList(2);
        actual = app.merge(l1, l2);
        expected = Arrays.asList(1,2,3,4,5,6,7,8,9);

    }
}
