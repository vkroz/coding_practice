package mini_challenges;

import java.util.LinkedList;

/**
 * Implement hashtable
 */
public class Hashtable {

  class KV {
    String key;
    String value;

    public KV(String key, String value) {
      this.key = key;
      this.value = value;
    }
  }

  LinkedList<KV>[] data;
  int size;

  public Hashtable(int size) {
    this.data = new LinkedList[size];
    this.size = size;
  }

  String get(String key) {
    int k = key.hashCode() % size;
    if( data[k] == null ) {
      return null;
    }
    else {
      LinkedList<KV> bucket = data[k];
      if( bucket.size() == 1) {
        return bucket.element().value;
      }
      else {
        for(KV kv: bucket) {
          if( kv.key.equals(key) )
            return kv.value;
        }
      }
    }
    return null;
  }



  void put(String key, String value) {
    int k = key.hashCode() % size;
    if( data[k] == null ) {
      LinkedList<KV> bucket = new LinkedList<>();
      data[k] = bucket;
      bucket.add( new KV(key, value));
    }
    else {
      LinkedList<KV> bucket = data[k];
      for(KV kv: bucket) {
        if( kv.key.equals(key) )
          bucket.remove(kv);
      }
      KV kv = new KV(key, value);
      bucket.add(kv);
    }
  }


}
