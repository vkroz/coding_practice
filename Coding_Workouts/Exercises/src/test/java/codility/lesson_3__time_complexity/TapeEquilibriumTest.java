package codility.lesson_3__time_complexity;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by vkroz on 3/15/16.
 */
public class TapeEquilibriumTest {

  @Test
  public void testSolution3() throws Exception {
    TapeEquilibrium app = new TapeEquilibrium();

    assertEquals(app.solution(new int[]{-1000, -100, -200, -300}), 400);

  }


  @Test
  public void testSolution() throws Exception {
    TapeEquilibrium app = new TapeEquilibrium();

    assertEquals(app.solution(new int[]{3, 1, 2, 4, 3}), 1);

  }
  @Test
  public void testSolution2() throws Exception {
    TapeEquilibrium app = new TapeEquilibrium();

    assertEquals(app.solution(new int[]{-1, 1, 2}), 2);

  }
}
