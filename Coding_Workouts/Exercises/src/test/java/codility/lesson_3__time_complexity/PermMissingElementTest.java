package codility.lesson_3__time_complexity;


import org.junit.Assert;
import org.junit.Test;

/**
 * Created by vkroz on 3/23/16.
 */
public class PermMissingElementTest {

  @Test
  public void testSolution() throws Exception {
    PermMissingElement app = new PermMissingElement();

    Assert.assertEquals( app.solution(new int[] {1,2,3,5,6,7}), 4);

  }
}
