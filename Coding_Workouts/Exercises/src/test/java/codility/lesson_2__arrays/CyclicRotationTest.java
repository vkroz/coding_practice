package codility.lesson_2__arrays;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by vkroz on 3/7/16.
 */
public class CyclicRotationTest {

  @Test
  public void testSolution() throws Exception {
    CyclicRotation app = new CyclicRotation();
    {
      int[] a = new int[]{3, 8, 9, 7, 6};
      int k = 1;
      int[] expected = new int[]{6, 3, 8, 9, 7};
      int[] actual = app.solution(a, k);
      Assert.assertArrayEquals(expected, actual);
    }
    {
      int[] a = new int[]{3, 8, 9, 7, 6};
      int k = 3;
      int[] expected = new int[]{9, 7, 6, 3, 8};
      int[] actual = app.solution(a, k);
      Assert.assertArrayEquals(expected, actual);
    }
    {
      int[] a = new int[]{3};
      int k = 3;
      int[] expected = new int[]{3};
      int[] actual = app.solution(a, k);
      Assert.assertArrayEquals(expected, actual);
    }
  }
}
