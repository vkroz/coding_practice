package codility.lesson_6__sorting;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by vkroz on 4/4/16.
 */
public class DistinctTest {
  @Test
  public void solution() throws Exception {
    Distinct app = new Distinct();
    int cnt = app.solution(new int[] {1,1,2,2,7,6,5,4,1,7});
    Assert.assertEquals(cnt, 6);
  }

}
