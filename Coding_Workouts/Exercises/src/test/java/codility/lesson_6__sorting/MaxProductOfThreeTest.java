package codility.lesson_6__sorting;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by vkroz on 4/4/16.
 */
public class MaxProductOfThreeTest {
  @Test
  public void solution() throws Exception {

    MaxProductOfThree app = new MaxProductOfThree();

    int[] arr = new int[] {-5, 5, -5, 4};
    int product = 125;
    Assert.assertEquals(product, app.solution(arr));

    arr = new int[] {-3, 1, 2, -2, 5, 6};
    product = 60;
    Assert.assertEquals(product, app.solution(arr));

  }

}