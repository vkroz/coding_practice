package codility.lesson_4__counting_elements;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by vkroz on 3/25/16.
 */
public class MissingIntegerTest {

  @Test
  public void testSolution() throws Exception {
    MissingInteger app = new MissingInteger();
    int r = app.solution(new int[] {1,3,6,4,1,2});
    Assert.assertEquals(5, r);
  }
}
