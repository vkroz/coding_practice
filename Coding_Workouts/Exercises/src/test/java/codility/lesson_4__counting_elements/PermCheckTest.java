package codility.lesson_4__counting_elements;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by vkroz on 3/25/16.
 */
public class PermCheckTest {

  @Test
  public void testSolution() throws Exception {

    PermCheck app = new PermCheck();
    assertEquals( 1, app.solution(new int[] {4,1,3,2}));
    assertEquals( 0, app.solution(new int[] {4,1,3}));
  }
}
