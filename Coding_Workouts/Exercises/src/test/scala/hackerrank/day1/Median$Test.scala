package hackerrank.day1

import org.scalatest.FunSuite

class Median$Test1 extends FunSuite {

  test("IF an array have even number of items THEN median is a mean value of 2 elements in the middle ") {
    val arr = Array[Int](10,12,14,16,18,20)
    val median = Median.median(arr)
    assert(median == 15)

  }

  test("IF an array have odd number of elements THEN median is an element in the middle") {
    val arr = Array[Int](12,14,15,18,20)
    val median = Median.median(arr)
    assert(median == 15)
  }

  test("Median for one element array is value of this element") {
    val arr = Array[Int](12)
    val median = Median.median(arr)
    assert(median == 12)
  }

  test("Median for empty array is Nil") {
    val arr = Array[Int]()
    val median:Int = Median.median(arr)
    assert(median == 0)
  }

}

class Median$Test2 extends FunSuite {

  test("IF an array have even number of items THEN median is a mean value of 2 elements in the middle ") {
    val arr = Array[Int](10,12,14,16,18,20)
    val medianTuple = Median.medianTuple(arr)
    assert(medianTuple._1 == 15)
    println(arr)
    println(medianTuple._2)
    println(medianTuple._3)
    println("----------")
  }

  test("IF an array have odd number of elements THEN median is an element in the middle") {
    val arr = Array[Int](12,14,15,18,20)
    val medianTuple = Median.medianTuple(arr)
    assert(medianTuple._1 == 15)
    println(arr)
    println(medianTuple._2)
    println(medianTuple._3)
    println("----------")
  }

  test("Median for one element array is value of this element") {
    val arr = Array[Int](12)
    val medianTuple = Median.medianTuple(arr)
    assert(medianTuple._1 == 12)
    println(arr)
    println(medianTuple._2)
    println(medianTuple._3)
    println("----------")
  }

  test("Median for empty array is Nil") {
    val arr = Array[Int]()
    val medianTuple = Median.medianTuple(arr)
    assert(medianTuple._1 == 0)
    println(arr)
    println(medianTuple._2)
    println(medianTuple._3)
    println("----------")
  }

}
