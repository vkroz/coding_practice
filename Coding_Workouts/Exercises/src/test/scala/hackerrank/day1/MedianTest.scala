package hackerrank.day1

import org.junit.Assert._
import org.junit.{After, Before, Test}

class MedianTest1 {

  @Test
  def testEven() {
    val arr = Array[Int](10,12,14,16,18,20)
    val median = Median.median(arr)
    assert(median == 15)

  }

  @Test
  def testOdd() {
    val arr = Array[Int](12,14,15,18,20)
    val median = Median.median(arr)
    assert(median == 15)
  }

  @Test
  def testSingleElementArray() {
    val arr = Array[Int](12)
    val median = Median.median(arr)
    assert(median == 12)
  }

  @Test
  def testEmptyArray() {
    val arr = Array[Int]()
    val median:Int = Median.median(arr)
    assert(median == 0)
  }

  @Test
  def testTupleEven() {
    val arr = Array[Int](10,12,14,16,18,20)
    val medianTuple = Median.medianTuple(arr)
    assert(medianTuple._1 == 15)
    assertArrayEquals(medianTuple._2, Array(10,12,14))
    assertArrayEquals(medianTuple._3, Array(16,18,20))
  }

  @Test
  def testTupleOdd() {
    val arr = Array[Int](12,14,15,18,20)
    val medianTuple = Median.medianTuple(arr)
    assert(medianTuple._1 == 15)
    assertArrayEquals(medianTuple._2, Array(12,14))
    assertArrayEquals(medianTuple._3, Array(18,20))
  }

  @Test
  def testTupleSingleElementArray() {
    val arr = Array[Int](12)
    val medianTuple = Median.medianTuple(arr)
    assert(medianTuple._1 == 12)
    assertEquals(medianTuple._2.length, 0)
    assertEquals(medianTuple._3.length, 0)
  }

}
