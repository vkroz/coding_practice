package interviewcake;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Ex31__recursive_string_permutations {

    /**
     * @return All permutations of given string
     * <p>
     * E.g.: complete set of permutations for string 'abc' is
     * <pre>
     *     "abc",
     *     "acb",
     *     "bac",
     *     "bca",
     *     "cab",
     *     "cba"
     * </pre>
     */
    public static Set<String> permutations(String str) {
        Set<String> retval = new HashSet<>();
        if(str.length() > 1) {
            for (int i = 0; i < str.length(); i++) {
                char ch = str.charAt(i);
                String substr = substr(str, i);
                Set<String> ap = permutations(substr);
                for (String perm : ap) {
                    retval.add("" + ch + perm);
                }
            }
        }
        else {
            retval = new HashSet<>(Arrays.asList(str));
        }
        return retval;
    }

    private static String substr(String str, int skipIdx) {
        char[] string = str.toCharArray();
        StringBuilder retval = new StringBuilder();
        int dstI = 0;
        for(int srcI=0; srcI<string.length; srcI++) {
            if( srcI != skipIdx)
                retval.append(string[srcI]);
        }
        return retval.toString();
    }

    @Test public void testPermutation() {
        Set<String> expected = new HashSet<>(Arrays.asList("abc", "acb", "bac", "bca", "cab", "cba"));
        assertThat(Ex31__recursive_string_permutations.permutations("abc"), is(expected));
    }
}
