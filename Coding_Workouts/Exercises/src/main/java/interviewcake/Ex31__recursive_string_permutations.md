Write a recursive function for generating all permutations of an input string. Return them as a set.
Don't worry about time or space complexity—if we wanted efficiency we'd write an iterative version.

To start, assume every character in the input string is unique.

Your function can have loops—it just needs to also be recursive.



```
           n!
P(n,k) =  ---
          (n-k)!
```


solution:

abc
----
a
    bc
    cb
b
    ac
    ca
c
    ab
    ba


abcd
-----
a   `bcd`
    b   `cd`
        c   `d`
            d
        d   `c`
            c
    c   `bd`

