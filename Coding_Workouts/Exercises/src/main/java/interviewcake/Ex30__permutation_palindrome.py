def has_palindrom(arg):
    """
    "civic"     should return True
    "ivicc"     should return True
    "civil"     should return False
    "livci"     should return False
    "aaa"       True
    "aa"        True
    "aaabbaaa"  True
    "aaabcbaaa" True
    """

    """
    count letters, 
    one or zero odd counters and other must be even 
    """
    char_counts = {}
    for ch in arg:
        if char_counts.has_key(ch):
            char_counts[ch] = char_counts[ch]+1
        else:
            char_counts[ch] = 1

    odd_count = 0
    for ch in char_counts.iterkeys():
        if char_counts[ch] % 2 != 0:
            if odd_count == 0:
                odd_count = 1
            else:
                return False

    return True


# run your function through some test cases here
# remember: debugging is half the battle!

for teststr in [ "acb",
    "civic", "ivicc", "civil", "livci", "aaa", "aa", "aaabbaaa", "aaabcbaaa" ]:
    print("some permutaion of '{}' is palindrom: {}'".format(teststr, has_palindrom(teststr)))
