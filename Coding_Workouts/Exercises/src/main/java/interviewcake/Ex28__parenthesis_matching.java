package interviewcake;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class Ex28__parenthesis_matching {

    /**
     * @return Position of corresponding closing paren
     */
    public static int matchingParens(String str, int openParenIdx) {
        // parens counter = 0
        // start from specified position
        // read characters,
        //  increment parens counter on opening parens
        //  decrement parens counter on closing parens
        //  stop when parens counter is 0, return this positon

        int parensCounter = 0;
        for (int i = openParenIdx; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch == '(') {
                parensCounter++;
            } else if (ch == ')') {
                parensCounter--;
            }
            if (parensCounter == 0)
                return i;
        }
        return -1;
    }

    @Test public void testMatchingParens() {
        assertThat(Ex28__parenthesis_matching.matchingParens(
                "Sometimes (when I nest them (my parentheticals) too much (like this (and this))) they get confusing.",
                10), is(79));
    }
}
