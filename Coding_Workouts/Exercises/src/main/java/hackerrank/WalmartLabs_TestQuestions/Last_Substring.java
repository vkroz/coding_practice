package hackerrank.WalmartLabs_TestQuestions;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * Given the string s = "ab", we find the alphabetically-ordered set of substrings of s to be
 * {"a", "ab", "b"}.
 * The alphabetically last substring (i.e., the substring that occurs last when all the substrings
 * are ordered alphabetically) of s is "b".
 * <p>
 * Example
 * s = "banana"
 * The alphabetically-ordered set of unique substrings of s is
 * {"a", "an", "ana", "anan", "anana", "b", "ba", "ban", "bana", "banan", "banana", "n", "na", "nan", "nana"}.
 * We return the alphabetically last one, which is "nana".
 */
public class Last_Substring {

    static String compute(String s) {
        Set<String> bucket = new HashSet<>();

        for(int start=0; start < s.length(); start++ ) {
            for(int end=start+1; end <= s.length(); end++) {
                String substring = s.substring(start, end);
                bucket.add(substring);
            }
        }

        List<String> substrings = new ArrayList<String>(bucket);
        Collections.sort(substrings);
        String last = substrings.get(substrings.size()-1);
        return last;
    }

    @Test public void test1() {
        String s = "ba";
        Assert.assertEquals("ba", Last_Substring.compute(s));

        s = "banana";
        Assert.assertEquals("nana", Last_Substring.compute(s));
    }
}
