package hackerrank.algorithms.warmup;

import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

import static org.hamcrest.core.Is.is;

public class Diagonal_Difference {

    @Test public void testTimeConversion() {
        /*
        3
11 2 4
4 5 6
10 8 -12
         */
        int[][] arr = {{11, 2, 4}, {4, 5, 6}, {10, 8, -12}};
        Assert.assertThat(Diagonal_Difference.diagonalDifference(arr), is(15));
    }

    static int diagonalDifference(int[][] arr) {
        int leftSum = 0;
        int rightSum = 0;
        for (int i = 0; i < arr.length; i++) {
            leftSum += arr[i][i];
            rightSum += arr[arr.length - 1 - i][i];
        }

        return Math.abs(leftSum - rightSum);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[][] a = new int[n][n];
        for (int a_i = 0; a_i < n; a_i++) {
            for (int a_j = 0; a_j < n; a_j++) {
                a[a_i][a_j] = in.nextInt();
            }
        }
        int result = diagonalDifference(a);
        System.out.println(result);
        in.close();
    }
}
