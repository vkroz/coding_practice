package codility.lesson_4__counting_elements;

import java.util.HashSet;

/**
 * PermCheck
 * Check whether array A is a permutation
 *
 * A permutation is a sequence containing each element from 1 to N once, and only once
 */
public class PermCheck {
  /**
   * @param A A.length <= 100000,
   * @return 1 if permutation, 0 is not
   */
  public int solution(int[] A) {
    HashSet<Integer> keys = new HashSet();
    for(int i=0; i< A.length; i++) {
      int n = A[i];
      if( n > A.length)
        return 0;
      if( keys.contains(n))
        return 0;
      keys.add(n);
    }
    return (keys.size() == A.length) ? 1: 0;
  }

}
