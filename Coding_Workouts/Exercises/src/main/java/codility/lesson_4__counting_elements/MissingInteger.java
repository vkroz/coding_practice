package codility.lesson_4__counting_elements;

import java.util.HashSet;
import java.util.Set;

/**
 * MissingInteger
 * Find the minimal positive integer not occurring in a given sequence
 */
public class MissingInteger {
  public int solution(int[] A) {
    Set<Integer> set = new HashSet<Integer>();
    for( int n: A) {
      set.add(n);
    }

    int min_max = 0;
    for( int i = 1; i < Integer.MAX_VALUE; i++ ) {
      if( ! set.contains(i)) {
        min_max = i;
        break;
      }
    }
    return min_max;
  }
}
