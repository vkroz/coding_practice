package codility.lesson_15__dynamic_programming;

import org.junit.Assert;
import org.junit.Test;

public class DynamicCoinChanging {

    /**
     * Returns minimum number of coins
     */
    int solution(int[] denominations, int value) {
        int[][] c = new int[denominations.length+1][value + 1];
        for (int j = 0; j < value + 1; j++)
            c[0][j] = Integer.MAX_VALUE;
        for (int i = 0; i < denominations.length; i++)
            c[i][0] = 0;

        System.out.println("Denoms: ");
        IntsToString(denominations);
        System.out.println("Value:" + value);

        int min = Integer.MAX_VALUE;
        for (int i = 1; i < denominations.length+1; i++) {
            int dn = denominations[i-1];
            for (int j = 1; j < value + 1; j++) {
                if (dn > j) {
                    c[i][j] = c[i - 1][j];
                } else {
                    int v = j;
                    int w = v / dn;
                    int r = v % dn;
                    if (r == 0) {
                        c[i][j] = w;
                    } else {
                        c[i][j] = w + c[i][r];
                    }
                }
            }
            min = Math.min(min, c[i][value]);
        }
        IntsToString(denominations, c);
        System.out.println("Result=" + min + "\n\n");
        return min;
    }


    static void IntsToString(int[] dn, int[][] a) {
        System.out.printf("\n    ");
        for (int j = 0; j < a[0].length; j++) {
            System.out.printf("%2d ", j);
        }
        System.out.printf("\n----------------\n");
        for (int i = 0; i < a.length; i++) {
            if( i == 0 )
                System.out.printf("%d [ ", 0);
            else
                System.out.printf("%d [ ", dn[i-1]);
            for (int j = 0; j < a[i].length; j++) {
                if (a[i][j] < Integer.MAX_VALUE)
                    System.out.printf("%2d ", a[i][j]);
                else
                    System.out.printf("%2s ", "xx");
            }
            System.out.printf("]\n");
        }
        System.out.printf("----------------\n");
    }

    static void IntsToString(int[]a) {
        System.out.printf("[ ");
        for (int i = 0; i < a.length; i++) {
            System.out.printf("%d ", a[i]);
        }
        System.out.printf("]\n");
    }

    @Test
    public void solution() throws Exception {
        DynamicCoinChanging app = new DynamicCoinChanging();
        Assert.assertEquals(2, app.solution(new int[]{1, 2, 3}, 6));
        Assert.assertEquals(4, app.solution(new int[]{1, 2, 3}, 10));
        Assert.assertEquals(3, app.solution(new int[]{1, 3}, 5));
        Assert.assertEquals(4, app.solution(new int[]{1, 3}, 8));

    }


}
