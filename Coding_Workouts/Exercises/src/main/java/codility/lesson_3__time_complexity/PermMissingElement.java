package codility.lesson_3__time_complexity;

/**
 * Created by vkroz on 3/23/16.
 */
public class PermMissingElement {
  public int solution(int[] A) {
    long N = A.length + 1;
    long total = N * (N + 1) / 2;
    long sum = 0L;
    for (final int i : A) {
      sum += i;
    }
    return (int) (total - sum);
  }
}
