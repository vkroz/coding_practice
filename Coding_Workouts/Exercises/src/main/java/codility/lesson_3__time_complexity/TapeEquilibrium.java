package codility.lesson_3__time_complexity;

/**
 * Minimize the value |(A[0] + ... + A[P-1]) - (A[P] + ... + A[N-1])|.
 */
public class TapeEquilibrium {
  public int solution(int[] A) {

    // First time count sum of A[0]..A[N]
    int sum = 0;
    for (int i = 0; i < A.length; i++) {
      sum += A[i];
    }

    // Now lets increment P from 1 to N-1, and for each value of P compute difference between and max diiference

    int left_sum = 0;
    int right_sum = sum;
    int diff = Math.abs(sum);
    int min_diff = diff;
    for (int p = 1; p < A.length - 1; p++) {
      left_sum += A[p - 1];
      right_sum -= A[p - 1];
      diff = Math.abs(left_sum - right_sum);
      min_diff = Math.min( min_diff, diff);
    }

    return min_diff;
  }
}
