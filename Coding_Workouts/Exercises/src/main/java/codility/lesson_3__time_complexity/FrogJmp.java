package codility.lesson_3__time_complexity;

import org.junit.Assert;
import org.junit.Test;

/**
 * FrogJmp
 * Count minimal number of jumps from position X to Y.
 * <p/>
 * A small frog wants to get to the other side of the road. The frog is currently located at position X and wants to get to a position greater than or equal to Y. The small frog always jumps a fixed distance, D.
 * <p/>
 * Count the minimal number of jumps that the small frog must perform to reach its target.
 * <p/>
 * Write a function:
 * <p/>
 * class Ex31__recursive_string_permutations { public int solution(int X, int Y, int D); }
 * <p/>
 * that, given three integers X, Y and D, returns the minimal number of jumps from position X to a position equal to or greater than Y.
 * <p/>
 * For example, given:
 * <p/>
 * X = 10
 * Y = 85
 * D = 30
 * the function should return 3, because the frog will be positioned as follows:
 * <p/>
 * after the first jump, at position 10 + 30 = 40
 * after the second jump, at position 10 + 30 + 30 = 70
 * after the third jump, at position 10 + 30 + 30 + 30 = 100
 * Assume that:
 * <p/>
 * X, Y and D are integers within the range [1..1,000,000,000];
 * X ≤ Y.
 * Complexity:
 * <p/>
 * expected worst-case time complexity is O(1);
 * expected worst-case space complexity is O(1).
 */
public class FrogJmp {

    public int solution(int X, int Y, int D) {
        int j = (Y - X) / D;
        int r = (Y - X) % D;
        int o = j + ((r > 0) ? 1 : 0);
        return o;
    }

    @Test public void testSolution() throws Exception {
        FrogJmp app = new FrogJmp();
        Assert.assertEquals(app.solution(10, 85, 30), 3);
    }

}
