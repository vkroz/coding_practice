package codility.lesson_8__leader.Dominator;

import org.junit.Assert;
import org.junit.Test;

public class Solution {

    public int solution(int[] A) {
        if( A.length == 0 )
            return -1;

        // Determine if any of elements
        Integer val = null;
        int size = 0;
        for (int i = 0; i < A.length; i++) {
            if (size == 0) {
                val = A[i];
                size++;
            }
            else {
                if( A[i] == val) size++;
                else size--;
            }
        }


        int candidate = val;
        int cnt = 0;
        int idx = -1;
        for (int i = 0; i < A.length; i++) {
            if( A[i] == candidate) {
                cnt++;
                idx = i;
            }
        }
        if( cnt > A.length/2)
            return idx;
        else
            return -1;
    }

    @Test
    public void solution() {
        Solution app = new Solution();
        Assert.assertEquals(-1, app.solution(new int[]{}));
        Assert.assertEquals(0, app.solution(new int[]{4}));
        Assert.assertEquals(-1, app.solution(new int[]{2, 1, 1, 3, 4}));
        Assert.assertEquals(4, app.solution(new int[]{0,0,1,1,1}));
        Assert.assertEquals(6, app.solution(new int[]{3, 2, 3, 4, 3, 3, 3, -1}));
    }

}

