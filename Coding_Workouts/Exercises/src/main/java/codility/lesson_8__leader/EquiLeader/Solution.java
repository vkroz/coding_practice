package codility.lesson_8__leader.EquiLeader;

import org.junit.Assert;
import org.junit.Test;

public class Solution {

    public int solution(int[] A) {
        // Find leader
        Integer leader = findLeader(A);
        if (leader==null)
            return 0;

        int totalLeadersCnt = 0;
        for (int a : A) {
            if (a == leader)
                totalLeadersCnt++;
        }

        int equiLeaders=0;
        int leadersCntLeft=0;
        for (int i = 0; i < A.length; i++) {
            if( A[i] == leader) {
                leadersCntLeft++;
            }
            int leadersCntRight = totalLeadersCnt - leadersCntLeft;
            int lenLeft = i+1;
            int lenRight = A.length - lenLeft;
            if( leadersCntLeft > lenLeft/2 && leadersCntRight > lenRight/2)
                equiLeaders++;
        }
        return equiLeaders;
    }

    public Integer findLeader(int[] A) {
        // Find leader
        Integer value = null;
        int cnt = 0;
        for (int i = 0; i < A.length; i++) {
            if (value == null)
                value = A[i];

            if (A[i] != value) {
                cnt = cnt > 0 ? cnt - 1 : 0;
                if (cnt == 0)
                    value = null;
            } else {
                cnt++;
            }
        }
        return (cnt > 0) ? value : null;
    }

    @Test
    public void solution() {
        Solution app = new Solution();
        Assert.assertEquals(2, app.solution(new int[]{4, 3, 4, 4, 4, 2}));
    }

}

