package codility.lesson_5__prefix_sums;

/**
 * Count the number of passing cars on the road.
 */
public class CountDiv {
  public int solution(int[] A) {
    int p_cnt = 0;
    int q_cnt = 0;
    int cnt = 0;
    for (int i = 0; i < A.length; i++) {
      if (A[i] == 0) {
        p_cnt++;
      } else {
        q_cnt++;
        if (p_cnt > 0) {
          cnt += p_cnt;
          if (cnt > 1000000000)
            return -1;
        }
      }
    }
    return cnt;
  }
}
