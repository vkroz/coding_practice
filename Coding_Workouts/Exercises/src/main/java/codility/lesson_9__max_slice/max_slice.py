"""
"""


def solution_o3(A):
    """
    Brute force
    O(n**3)
    """
    n = len(A)
    max_slice = None
    for p in xrange(n):
        for q in xrange(p, n):
            slice = 0
            for i in xrange(p, q + 1):
                slice += A[i]

            if max_slice is None:
                max_slice = slice
            else:
                max_slice = max(max_slice, slice)

    return max_slice


def solution_o2_1(A):
    """
    Ex31__recursive_string_permutations with prefix sums
    """
    n = len(A)
    pref = [A[0]] * n
    for i in xrange(1, n):
        pref[i] = pref[i - 1] + A[i]

    max_slice = None
    for p in xrange(n):
        for q in xrange(p, n):
            slice = pref[q] - pref[p - 1] if p > 0 else 0
            if max_slice is None:
                max_slice = slice
            else:
                max_slice = max(max_slice, slice)

    return max_slice


def solution_o2_2(A):
    """
    Args:
        a:  list of integers

    Returns:
        Max slice array
    """
    n = len(A)
    max_slice = None
    for p in xrange(n):
        slice = 0
        for q in xrange(p, n):
            slice += A[q]
            if max_slice is None:
                max_slice = slice
            else:
                max_slice = max(max_slice, slice)

    return max_slice


def solution_o_n(A):
    """
    For each position, we compute the largest sum that ends in that position
    Algorithm works correctly if at least one positive number is present

    I.e.

    A[i]    i       max ending here     max so far
    ----    ---     ---------------     -------------
    5       0                    5                  5
    -7      1       max(-7,-2)= -2      max(-2,5) = 5
    3       2       max(3,1)=    3      max(5,3)  = 5
    5       3       max(5,8)=    8      max(5,8)  = 8
    -2      4       max(-2,6)=   6      max(8,6)  = 8
    4       5       max(4,10)=  10      max(8,10) = 10
    -1      6       max(-1,9)=   9      max(10,9) = 10
    """
    max_ending_here = max_so_far = A[0]
    for x in A[1:]:
        max_ending_here = max(x, max_ending_here + x)
        max_so_far = max(max_so_far, max_ending_here)
    return max_so_far

def solution_o_n2(A):
    """
    For each position, we compute the largest sum that ends in that position
    Algorithms allows to have all negative numbers

    A[i]    p    q       slice                   max_slice
    ----    ---  ---     --------------------    ----------
    5       0    0..6    5, -2, 1, 6, 4, 8, 7    8
    -7      1    1..6    -7, -4, 1, -1, 3, 2     8
    3       2    2       3, 8, 6, 10, -1         10
    5       3    3       5, 3, 7, 6              10
    -2      4    4       -2, 2, 3                10
    4       5    5       4, 3                    10
    -1      6    6       -1                      10
    """
    n = len(A)
    max_slice = None
    for p in xrange(n):
        slice = 0
        for q in xrange(p, n):
            slice += A[q]
            if max_slice is None:
                max_slice = slice
            else:
                max_slice = max(max_slice, slice)

    return max_slice

print "---O(n) --"
ms = solution_o_n([5, -7, 3, 5, -2, 4, -1])
print ms
assert ms == 10
print ""

print "---O(n**3)--"
ms = solution_o3([5, -7, 3, 5, -2, 4, -1])
print ms
assert ms == 10
print ""

print "---O(n**2)--"
ms = solution_o2_1([5, -7, 3, 5, -2, 4, -1])
print ms
assert ms == 10
print ""

print "---O(n**2) v2--"
ms = solution_o2_2([5, -7, 3, 5, -2, 4, -1])
print ms
assert ms == 10
print ""

