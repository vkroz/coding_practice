package codility.lesson_9__max_slice;

import org.junit.Test;

import static java.lang.Math.max;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * For each position, we compute the largest sum that ends in that position
 * Algorithm works correctly if at least one positive number is present
 * <p>
 * I.e.
 * <p>
 * A[i]    i       max ending here     max slice
 * ----    ---     ---------------     -------------
 * 5       0                    5                  5
 * -7      1       max(-7,-2)= -2      max(-2,5) = 5
 * 3       2       max(3,1)=    3      max(5,3)  = 5
 * 5       3       max(5,8)=    8      max(5,8)  = 8
 * -2      4       max(-2,6)=   6      max(8,6)  = 8
 * 4       5       max(4,10)=  10      max(8,10) = 10
 * -1      6       max(-1,9)=   9      max(10,9) = 10
 * <p>
 */
public class MaxSlice {
    public int maxSliceSlow(int[] arr) {
        int maxSlice = Integer.MIN_VALUE;
        for (int start = 0; start < arr.length; start++) {
            for (int end = start + 1; end <= arr.length; end++) {
                int slice = 0;
                for (int i = start; i < end; i++) {
                    slice = slice + arr[i];
                }
                maxSlice = max(maxSlice, slice);
            }
        }
        return maxSlice;
    }

    public int maxSlice(int[] arr) {
        int maxEnding = 0;
        int maxSlice = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            maxEnding = max(arr[i], maxEnding + arr[i]);
            maxSlice = max(maxSlice, maxEnding);
        }
        return maxSlice;
    }

    @Test public void testSlow() {
        assertThat(maxSliceSlow(new int[] {-1, -2, -3, -4, -5}), is(-1));
        assertThat(maxSliceSlow(new int[] {5, -7, 3, 5, -2, 4, 1}), is(11));
        assertThat(maxSliceSlow(new int[] {15, -7, 3, 5, -2, 4, 1}), is(19));
        assertThat(maxSliceSlow(new int[] {5, -7, 3, 5, -2, 4, 10}), is(20));
        assertThat(maxSliceSlow(new int[] {1, 2, 3}), is(6));
    }

    @Test public void test() {
        assertThat(maxSlice(new int[] {-1, -2, -3, -4, -5}), is(-1));
        assertThat(maxSlice(new int[] {5, -7, 3, 5, -2, 4, 1}), is(11));
        assertThat(maxSlice(new int[] {15, -7, 3, 5, -2, 4, 1}), is(19));
        assertThat(maxSlice(new int[] {5, -7, 3, 5, -2, 4, 10}), is(20));
        assertThat(maxSlice(new int[] {1, 2, 3}), is(6));
    }

}
