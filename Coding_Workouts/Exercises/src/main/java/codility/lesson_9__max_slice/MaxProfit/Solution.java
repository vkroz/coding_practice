package codility.lesson_9__max_slice.MaxProfit;

import org.junit.Assert;
import org.junit.Test;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class Solution {
    /**
     * """
     * For each position, we compute the largest sum that ends in that position
     * Algorithm works correctly if at least one positive number is present
     * <p>
     * I.e.
     * <p>
     * A[i]    i       max ending here     max slice
     * ----    ---     ---------------     -------------
     * 5       0                    5                  5
     * -7      1       max(-7,-2)= -2      max(-2,5) = 5
     * 3       2       max(3,1)=    3      max(5,3)  = 5
     * 5       3       max(5,8)=    8      max(5,8)  = 8
     * -2      4       max(-2,6)=   6      max(8,6)  = 8
     * 4       5       max(4,10)=  10      max(8,10) = 10
     * -1      6       max(-1,9)=   9      max(10,9) = 10
     * """
     * max_ending_here = max_so_far = A[0]
     * for x in A[1:]:
     * max_ending_here = max(x, max_ending_here + x)
     * max_so_far = max(max_so_far, max_ending_here)
     * return max_so_far
     */
    int solution2(int[] A) {
        if (null == A || A.length == 0)
            return 0;

        int minPrice = A[0];
        int maxDelta = 0;
        System.out.println(format("A[%d]=%d, minPrice=%d, maxDelta=%d", 0, A[0], minPrice, maxDelta));
        for (int i = 1; i < A.length; i++) {
            if( minPrice > A[i])
                minPrice = A[i];

            int delta = A[i] - minPrice;
            maxDelta = Math.max(maxDelta, delta);
            System.out.println(format("A[%d]=%d, minPrice=%d, maxDelta=%d", i, A[i], minPrice, maxDelta));
        }
        System.out.println("----------------");
        return maxDelta;
    }

    int solution(int[] A) {

        int maxEnd = 0;
        int maxSlice = 0;

        if (A.length < 2) return 0;

        for (int i = 1; i < A.length; i++) {
            int delta = A[i] - A[i - 1];
            maxEnd = Math.max(0, maxEnd + delta);
            maxSlice = Math.max(maxSlice, maxEnd);
            System.out.println(format("A[%d]=%d, delta=%d, maxEnd=%d, maxSlice=%d", i, A[i], delta, maxEnd, maxSlice));
        }
        System.out.println("----------------");

        if (maxSlice < 0) return 0;

        return maxSlice;
    }

    @Test
    public void testSolution() {
        Solution app = new Solution();


        assertThat(app.solution(new int[]{23171, 21011, 21123, 21366, 21013, 21367}), equalTo(356));
        assertThat(app.solution(new int[]{4, 2, 3, 6, 7, 5}), equalTo(5));

        assertThat(app.solution(new int[]{}), equalTo(0));
        assertThat(app.solution(new int[]{1, 2, 3, 2, 3, 4, 5, 4}), equalTo(4));
        assertThat(app.solution(new int[]{4, 2, 3, 6, 7, 5}), equalTo(5));
        assertThat(app.solution(new int[]{7,5,9,6,11,1,10,6}), equalTo(9));


    }

//    @Test
    public void testSolution2() {
        Solution app = new Solution();


        assertThat(app.solution2(new int[]{23171, 21011, 21123, 21366, 21013, 21367}), equalTo(356));
        assertThat(app.solution2(new int[]{4, 2, 3, 6, 7, 5}), equalTo(5));

        assertThat(app.solution2(new int[]{}), equalTo(0));
        assertThat(app.solution2(new int[]{1, 2, 3, 2, 3, 4, 5, 4}), equalTo(4));
        assertThat(app.solution2(new int[]{4, 2, 3, 6, 7, 5}), equalTo(5));
        assertThat(app.solution2(new int[]{7,5,9,6,11,1,10,6}), equalTo(9));


    }

}

