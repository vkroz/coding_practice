package codility.lesson_6__sorting;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Write a function
 * <p>
 * int solution(int A[], int N);
 * <p>
 * that, given a zero-indexed array A consisting of N integers, returns the number of distinct
 * values in array A.
 */
public class Distinct {
  int solution(int[] A) {
    Integer[] aa = Arrays.stream( A ).boxed().toArray( Integer[]::new );
    Set<Integer> set = new HashSet<Integer>(Arrays.asList(aa));
    return set.size();
  }
}
