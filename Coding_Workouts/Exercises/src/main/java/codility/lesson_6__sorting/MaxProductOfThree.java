package codility.lesson_6__sorting;

/**
 * A non-empty zero-indexed array A consisting of N integers is given. The product of triplet (P, Q,
 * R) equates to A[P] * A[Q] * A[R] (0 ≤ P < Q < R < N).
 * <p>
 * For example, array A such that:
 * <p>
 * A[0] = -3 A[1] = 1 A[2] = 2 A[3] = -2 A[4] = 5 A[5] = 6 contains the following example triplets:
 * <p>
 * (0, 1, 2), product is −3 * 1 * 2 = −6 (1, 2, 4), product is 1 * 2 * 5 = 10 (2, 4, 5), product is
 * 2 * 5 * 6 = 60 Your goal is to find the maximal product of any triplet.
 */
public class MaxProductOfThree {
  void show(Integer[] a) {
    for(int i: a) {
      System.out.printf("%d ", i);
    }
    System.out.printf("\n");
  }

  public int solution(int[] A) {
    int N = A.length;
    quicksort(A, 0, N - 1);

    int tmp1 = (A[0] * A[1]);
    int tmp2 = (A[N - 2] * A[N - 3]);

    if (A[N - 1] > 0) {
      return A[N - 1] * ((tmp1 > tmp2) ? tmp1 : tmp2);
    } else {
      return A[N - 1] * ((tmp1 < tmp2) ? tmp1 : tmp2);
    }
  }

  private void quicksort(int arr[], int left, int right) {
    int index = partition(arr, left, right);

    if (left < index - 1) {
      quicksort(arr, left, index - 1);
    }

    if (index < right) {
      quicksort(arr, index, right);
    }
  }

  private int partition(int arr[], int left, int right) {
    int pivot = arr[(left + right) / 2];

    while (left <= right) {
      while (arr[left] < pivot) {
        left++;
      }

      while (arr[right] > pivot) {
        right--;
      }

      if (left <= right) {
        int tmp = arr[left];
        arr[left++] = arr[right];
        arr[right--] = tmp;
      }
    }

    return left;
  }
}
