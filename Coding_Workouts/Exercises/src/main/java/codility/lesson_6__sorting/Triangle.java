package codility.lesson_6__sorting;

import java.util.Arrays;

/**
 * A zero-indexed array A consisting of N integers is given. A triplet (P, Q, R) is triangular if 0
 * ≤ P < Q < R < N and:
 * <p>
 * A[P] + A[Q] > A[R], A[Q] + A[R] > A[P], A[R] + A[P] > A[Q]. For example, consider array A such
 * that:
 * <p>
 * A[0] = 10    A[1] = 2    A[2] = 5 A[3] = 1     A[4] = 8    A[5] = 20 Triplet (0, 2, 4) is
 * triangular.
 * <p>
 * Write a function:
 * <p>
 * class Ex31__recursive_string_permutations { public int solution(int[] A); }
 * <p>
 * that, given a zero-indexed array A consisting of N integers, returns 1 if there exists a
 * triangular triplet for this array and returns 0 otherwise.
 */
public class Triangle {

  public int solution(int[] A) {
    Arrays.sort(A);
    for(int i=0; i<A.length-2; i++) {
      if(A[i]+A[i+1] > A[i+2])
        return 1;
    }
    return 0;
  }

}
