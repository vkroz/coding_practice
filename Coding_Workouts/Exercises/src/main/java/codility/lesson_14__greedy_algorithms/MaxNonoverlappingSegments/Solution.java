package codility.lesson_14__greedy_algorithms.MaxNonoverlappingSegments;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;

import static org.junit.Assert.assertEquals;

public class Solution {

    class Segment {
        Segment(int a, int b) {
            this.a = a;
            this.b = b;
        }

        int a;
        int b;

        boolean isOverlap(Segment o) {
            return (this.a <= o.a && o.a <= this.b) || (o.a <= this.a && this.a <= o.b);
        }

        @Override
        public String toString() {
            return "Segment{" +
                    "a=" + a +
                    ", b=" + b +
                    '}';
        }
    }

    public int solution(int[] A, int[] B) {
        Deque<Segment> segs = new ArrayDeque<>();

        for (int i = 0; i < A.length; i++) {
            Segment curr = new Segment(A[i], B[i]);
            if (segs.size() == 0) {
                segs.addLast(curr);
            } else {
                Segment last = segs.getLast();
                if (!curr.isOverlap(last)) {
                    segs.addLast(curr);
                }
            }
        }
        System.out.println(segs);
        return segs.size();
    }

    @Test
    public void testSolution() {

        Solution app = new Solution();

        int[] A = {1, 3, 7, 9, 9};
        int[] B = {5, 6, 8, 9, 10};

        assertEquals(app.solution(A, B), 3);


        A = new int[]{0, 0, 0, 2, 3, 3, 4, 5, 6, 6, 7};
        B = new int[]{0, 1, 1, 2, 3, 4, 4, 5, 6, 7, 7};

        assertEquals(app.solution(A, B), 7);

        A = new int[]{0, 1, 2, 0, 4, 2, 6, 6, 8, 6};
        B = new int[]{0, 2, 4, 4, 4, 6, 6, 8, 8, 9};

        assertEquals(app.solution(A, B), 5);

    }

}
