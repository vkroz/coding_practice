def greedyCoinChanging(M, k):
    n = len(M)
    result = []
    for i in xrange(n - 1, -1, -1):
        result += [(M[i], k // M[i])]
        k %= M[i]
    return result


if __name__ == '__main__':

    M = [1,2,5,10]

    print("{} = {}".format("greedyCoinChanging(M, 7)", greedyCoinChanging(M, 7)))
    print("{} = {}".format("greedyCoinChanging(M, 1)", greedyCoinChanging(M, 1)))
    print("{} = {}".format("greedyCoinChanging(M, 20)", greedyCoinChanging(M, 20)))
    print("{} = {}".format("greedyCoinChanging(M, 9)", greedyCoinChanging(M, 9)))

