package codility.lesson_14__greedy_algorithms.TieRopes;

import org.junit.Assert;
import org.junit.Test;

/**
 * TieRopes
 * Tie adjacent ropes to achieve the maximum number of ropes of length >= K.
 */
public class Solution {

    public int solution(int K, int[] A) {
        int cnt = 0;
        int len = 0;
        for (int i = 0; i < A.length; i++) {
            len += A[i];
            if (len >= K) {
                cnt++;
                len = 0;
            }
        }
        return cnt;
    }

    @Test
    public void testSolution() {

        Solution app = new Solution();

        int[] A = {1, 2, 3, 4, 1, 1, 3};
        int k = 4;

        Assert.assertEquals(app.solution(k, A), 3);


        A = new int[]{1, 1, 1, 2, 3, 4};
        k = 4;

        Assert.assertEquals(app.solution(k, A), 2);

    }

}
