package codility.lesson_2__arrays;

/**
 A zero-indexed array A consisting of N integers is given. Rotation of the array means that each element is shifted right by one index, and the last element of the array is also moved to the first place.

 For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7]. The goal is to rotate array A K times; that is, each element of A will be shifted to the right by K indexes.

 Write a function:

 class Ex31__recursive_string_permutations { public int[] solution(int[] A, int K); }

 that, given a zero-indexed array A consisting of N integers and an integer K, returns the array A rotated K times.

 For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function should return [9, 7, 6, 3, 8].

 Assume that:

 N and K are integers within the range [0..100];
 each element of array A is an integer within the range [−1,000..1,000].
 In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.

 */
public class CyclicRotation {
  public int[] solution(int[] A, int K) {
    // write your code in Java SE 8

    for(int i=0; i<K; i++) {
      int lastPos = A.length-1;
      int tmp = A[lastPos];
      for (int j = lastPos-1; j >= 0; j--) {
        A[j+1] = A[j];
      }
      A[0] = tmp;
     }

    return A;
  }

}
