package codility.lesson_7__stacks_and_queues.StoneWall;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;

public class Solution {
    public int solution(int[] H) {

        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(0);
        int stones = 0;
        for (int i = 0; i < H.length; i++) {

            if (H[i] > stack.peek()) {
                stack.push(H[i]);
                stones += 1;
            } else {
                // elements in stack >= then until H[i] - remove elements from stack
                // if found equal - do not increment counter
                // if didn't find equal - increment counter
                while (stack.peek() > H[i]) {
                    stack.pop();
                }
                if (H[i] > stack.peek()) {
                    stack.push(H[i]);
                    stones += 1;
                } else {
                    // if value in stack equals to H[i], that means we already counted thios stone
                    ;
                }

            }
        }
        return stones;

    }

    @Test
    public void solution() {
        Solution app = new Solution();
        int s = app.solution(new int[] {8,8,5,7,9,8,7,4,8});
        Assert.assertEquals(7, s);
    }

}

