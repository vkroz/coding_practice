package codility.lesson_7__stacks_and_queues.Nesting;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;

public class Solution {

    public int solution(String S) {
        int cnt=0;
        for(char ch: S.toCharArray()) {
            if( ch == '(' ) {
                cnt++;
            }
            else {
                cnt--;
                if( cnt < 0 )
                    return 0;
            }
        }
        return cnt==0? 1 : 0;
    }

    @Test
    public void solution() {
        Solution app = new Solution();

        Assert.assertEquals(0, app.solution("())"));
        Assert.assertEquals(1, app.solution("()"));
        Assert.assertEquals(1, app.solution("()()(())((()))"));
        Assert.assertEquals(0, app.solution("())("));
        Assert.assertEquals(0, app.solution("((())"));
        Assert.assertEquals(0, app.solution("(()))"));
        Assert.assertEquals(0, app.solution("("));
        Assert.assertEquals(0, app.solution(")"));
    }

}

