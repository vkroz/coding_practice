package codility.lesson_7__stacks_and_queues.Fish;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;

public class Solution {

    public int solution(int[] A, int[] B) {

        Deque<Integer> downstream = new ArrayDeque<>();
        int upCnt = 0;

        for( int i=0; i<A.length; i++) {

            int fishSize = A[i];
            int fishDir = B[i];

            if( fishDir == 1 ) {
                downstream.push(i);
            } else {
                if( downstream.size() == 0)
                    upCnt++;
                else {
                    boolean upWon = false;
                    while (downstream.size() > 0) {
                        if (fishSize > A[downstream.peek()]) {
                            downstream.pop();
                            upWon=true;
                        } else {
                            upWon=false;
                            break;
                        }
                    }
                    upCnt = upWon? upCnt+1: upCnt;
                }
            }
        }

        int cnt = upCnt + downstream.size();
        return cnt;
    }

/*


  A[0] = 4    B[0] = 0
  A[1] = 3    B[1] = 1
  A[2] = 2    B[2] = 0
  A[3] = 1    B[3] = 0
  A[4] = 5    B[4] = 0

---
  A[3] = 9    B[3] = 1
-  A[1] = 4    B[1] = 0
-  A[2] = 5    B[2] = 0
  A[4] = 3    B[4] = 1
-  A[5] = 2    B[5] = 0
-  A[6] = 1    B[6] = 0
  A[7] = 15   B[7] = 0
---
 */
    @Test
    public void solution() {
        Solution app = new Solution();
//        int s = app.solution(new int[] {4,3,2,1,5}, new int[] {0,1,0,0,0});
//        Assert.assertEquals(2, s);

        int s = app.solution(new int[] {9,4,5,3,15}, new int[] {1,0,0,1,0});
        Assert.assertEquals(1, s);

//        s = app.solution(new int[] {9,4,5,3,2,1,15}, new int[] {1,0,0,1,0,0,0});
//        Assert.assertEquals(1, s);
    }

}

