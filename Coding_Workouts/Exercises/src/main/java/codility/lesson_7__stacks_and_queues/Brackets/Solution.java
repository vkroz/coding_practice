package codility.lesson_7__stacks_and_queues.Brackets;

import org.junit.Assert;
import org.junit.Test;

import java.util.EmptyStackException;
import java.util.Stack;

import static org.junit.Assert.assertEquals;

/**
 * PAINLESS Brackets VIEW START Determine whether a given string of parentheses is properly nested.
 */
public class Solution {

    int solution(String S) {
        Stack<Character> stack = new Stack<>();
        for (char ch : S.toCharArray()) {
            if ("({[".indexOf(ch) >= 0) {
                stack.push(ch);
            } else {
                int closeIdx = ")}]".indexOf(ch);
                if (closeIdx >= 0) {
                    try {
                        char openCh = stack.pop();
                        if ("({[".indexOf(openCh) != closeIdx)
                            return 0;
                    } catch (EmptyStackException e) {
                        return 0;
                    }
                }
            }
        }

        return 1;
    }

    @Test
    public void solution() throws Exception {

        Solution app;
        app = new Solution();

        assertEquals(app.solution(""), 1);
        assertEquals(app.solution("{}"), 1);
        assertEquals(app.solution("({[]})"), 1);
        assertEquals(app.solution("({[]})({[]})({[]})"), 1);
        assertEquals(app.solution("())"), 0);
        assertEquals(app.solution(")("), 0);
        assertEquals(app.solution("())"), 0);
        assertEquals(app.solution("(}"), 0);
    }

}


