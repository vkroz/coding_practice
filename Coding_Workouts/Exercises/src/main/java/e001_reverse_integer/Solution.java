package e001_reverse_integer;

import org.junit.Assert;
import org.junit.Test;

public class Solution {
  public int reverse(int x) {

    int srcNum = Math.abs(x);
    int result = 0;
    while (srcNum > 0) {
      int part = srcNum % 10;
      long result1 = (long) result * 10 + part;
      if (result1 < Integer.MAX_VALUE) {
        result = (int) result1;
        srcNum = srcNum / 10;
      } else {
        return 0;
      }
    }
    return result * ((x >= 0) ? 1 : -1);
  }

  public int reverse1(int x) {

    int place = 1;
    int result = 0;

    int sign = (x > 0) ? 1 : -1;
    x = Math.abs(x);

    while (x / place > 0) {
      int part = (x / place) % 10;
      int result1 = result * 10 + part;
      if (result1 < result) {
        return 0;
      } else {
        result = result1;
        place *= 10;
      }
    }
    return result * sign;
  }

  @Test
  public void testReverse() {
    Solution solution = new Solution();
    Assert.assertEquals(0, solution.reverse(1534236469));
    Assert.assertEquals(-2143847412, solution.reverse(-2147483412));
    Assert.assertEquals(-4321, solution.reverse(-1234));

    Assert.assertEquals(21, solution.reverse(120));
    Assert.assertEquals(4321, solution.reverse(1234));
    Assert.assertEquals(12, solution.reverse(21));
    Assert.assertEquals(1, solution.reverse(10));
    Assert.assertEquals(21, solution.reverse(12));
  }
}
