package hackerrank.day1


import scala.collection.mutable.{HashMap, ListBuffer}

class ComputedValues(data: Array[Int]) {
  val _data = data.sorted

  def median: Double = {

    if( _data.length % 2 != 0 ) {
      return _data(_data.length/2).toDouble
    }
    else {
      val l = _data.length/2-1
      val r = _data.length/2
      return (_data(l)+_data(r))/(2.toDouble)
    }

  }

  def mean: Double = {
    _data.sum/_data.length.toDouble
  }

  def mode: Int = {
    // Make hashmap with counters
    val map = new HashMap[Int, Int]()
    for(item <- _data) {
      val cnt = map.getOrElse(item, 0) + 1
      map += (item -> cnt)
    }


    var modeList = new ListBuffer[Int]()
    var maxCnt = -1;
    for( (num,cnt) <- map ) {
      if( cnt > maxCnt ) {
        maxCnt = cnt
        modeList = new ListBuffer[Int]()
        modeList += num
      }
      else if( cnt == maxCnt ) {
        modeList += num
      }
      else {
        // found value less then max -- skip it
      }
    }
    // sort the list
    return modeList.min
  }
}

object ComputedValues {
  val devMode = true;

  def readFromData(): (Int, Array[Int]) = {
    val line1 = "2500"
    val line2 = "19325 74348 68955 98497 26622 32516 97390 64601"
    val expectedResult =
      """
        |49921.5
        |49253.5
        |2184
      """.stripMargin

    val n = line1.toInt
    val arr = line2.split(" ").map(_.toInt)
    (n, arr)
  }

  def readFromStdIn(): (Int, Array[Int]) = {
    val numElements = scala.io.StdIn.readLine.toInt
    val elementArr = scala.io.StdIn.readLine.split(" ").map(_.toInt)
    return (numElements, elementArr)
  }


  def main(args: Array[String]) {

    // Get data
    val inputArray = if (devMode) readFromData()._2 else readFromStdIn()._2

    val result = new ComputedValues(inputArray)
    printf("%.1f\n", result.mean)
    printf("%.1f\n", result.median)
    printf("%d\n", result.mode)
  }

}
