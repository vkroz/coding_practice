package hackerrank.day1

/**
  * Created by vkroz on 2/2/17.
  */
object Median {

  def median(arr: Array[Int]): Int = {
      if( arr.length == 0 )
        return null.asInstanceOf[Int]

      val arrSorted = arr.sorted
      if( arrSorted.length % 2 != 0 ) {
        return arrSorted(arrSorted.length/2)
      }
      else {
        return (arrSorted(arrSorted.length/2-1)+arrSorted(arrSorted.length/2))/2
      }
  }

  /**
    * @param arr
    * @return Tuple: (median, array left, array right)
    */
  def medianTuple(arr: Array[Int]): (Int, Array[Int], Array[Int]) = {

    val arrSorted = arr.sorted

    if( arrSorted.length % 2 != 0 ) {
      // even length: split array exactly on halves
      val medianIdx = arrSorted.length/2
      val median = arrSorted(arrSorted.length/2)
      val arrayLeft = arrSorted.slice(0, Math.max(0, medianIdx))
      val arrayRight = arrSorted.slice(medianIdx+1, arrSorted.length)
      return (median, arrayLeft, arrayRight)
    }
    else {
      // even length: exclude median, retuen arrays on left and right
      val medianIdx = arrSorted.length/2
      val median = (arrSorted(arrSorted.length/2-1)+arrSorted(arrSorted.length/2))/2
      val arrayLeft = arrSorted.slice(0, Math.max(0, medianIdx))
      val arrayRight = arrSorted.slice(medianIdx, arrSorted.length)
      return (median, arrayLeft, arrayRight)
    }
  }


}
