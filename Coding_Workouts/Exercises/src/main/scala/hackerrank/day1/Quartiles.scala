package hackerrank.day1

object Quartiles {

  val devMode = false;

  /**
    * @param arr
    * @return Tuple: (median, array left, array right)
    */
  def medianTuple(arr: Array[Int]): (Int, Array[Int], Array[Int]) = {

    val arrSorted = arr.sorted

    if( arrSorted.length % 2 != 0 ) {
      // even length: split array exactly on halves
      val medianIdx = arrSorted.length/2
      val median = arrSorted(arrSorted.length/2)
      val arrayLeft = arrSorted.slice(0, Math.max(0, medianIdx))
      val arrayRight = arrSorted.slice(medianIdx+1, arrSorted.length)
      return (median, arrayLeft, arrayRight)
    }
    else {
      // even length: exclude median, retuen arrays on left and right
      val medianIdx = arrSorted.length/2
      val median = (arrSorted(arrSorted.length/2-1)+arrSorted(arrSorted.length/2))/2
      val arrayLeft = arrSorted.slice(0, Math.max(0, medianIdx))
      val arrayRight = arrSorted.slice(medianIdx, arrSorted.length)
      return (median, arrayLeft, arrayRight)
    }
  }

  def readFromData(): (Int, Array[Int]) = {
    return (9, Array(3, 7, 8, 5, 12, 14, 21, 13, 18))
  }

  def readFromStdIn(): (Int, Array[Int]) = {
    val numElements = scala.io.StdIn.readLine.toInt
    val elementArr = scala.io.StdIn.readLine.split(" ").map(_.toInt)
    return (numElements, elementArr)
  }

  def main(args: Array[String]) {

    // data._1 = array length
    // data._2 = array elements
    val data = if (devMode) readFromData() else readFromStdIn()
    val inputArray = data._2

    val median = medianTuple(inputArray)
    val Q1 = medianTuple(median._2)._1;
    val Q2 = median._1;
    val Q3 = medianTuple(median._3)._1;

    println(Q1)
    println(Q2)
    println(Q3)

  }
}



