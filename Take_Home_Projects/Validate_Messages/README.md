# 'Take Home' coding task
Source: http://swstaticwebcontent.s3-website-us-east-1.amazonaws.com/exercise1.html

## Build and execute

This project uses Maven to build code and execute tests

    mvn clean test


## Assignment

We've built a new communication protocol that sends messages with a restricted syntax.
We need to write a function which determines whether a given message is syntactically valid or not.
Here are the rules:

- There are 15 valid characters in the protocol: the lower-case characters 'a' through 'j'
  and the uppercase characters 'Z', 'M', 'K', 'P', and 'Q'.
- Every lower-case character in isolation is a valid message, e.g., 'a' is a valid message.
- If σ is a valid message then so is Zσ.
- If σ and τ are valid messages then so are Mστ, Kστ, Pστ, and Qστ.

Write a function in the language of your choosing (out of: Python, PHP, JavaScript, Java, C/C++) to check whether messages are valid.

The input consists of a series of potential messages separated by whitespace and containing
only the 15 characters above.

The output consists of one line per potential messages, followed by VALID if the
message is valid and INVALID if it is invalid.

Example output:

    Qa INVALID
    Zj VALID
    MZca VALID
    Khfa INVALID

Bonus Problems:

We have decided to extend the protocol to accept an arbitrary number of valid messages.
If a message begins with a number, precisely that number of valid messages must follow. e.g.

    3aaa VALID
    2aaa INVALID
    2ZaMbb VALID
    K2aaa VALID
    10aaaaaaaaaa VALID

In the case of invalid messages, output more descriptive errors.
For valid messages, output a parse tree.
Propose a change/addition to this problem to make it more interesting.

Based on problem by Jesse Farmer

