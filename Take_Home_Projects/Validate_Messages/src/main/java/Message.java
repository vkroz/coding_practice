/**
 * Represents single message and parsing status
 */
public class Message {
    public Message(String msg, boolean isValid) {
        this.msg = msg;
        this.isValid = isValid;
    }

    String msg;
    boolean isValid;

    @Override
    public String toString() {
        return String.format("%s %s", msg, (isValid) ? "VALID" : "INVALID");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (isValid != message.isValid) return false;
        return msg.equals(message.msg);

    }

    @Override
    public int hashCode() {
        int result = msg.hashCode();
        result = 31 * result + (isValid ? 1 : 0);
        return result;
    }
}
