import java.util.*;

/**
 * Created by vkroz on 4/12/16.
 */
public class MessageReader {

    public List<Message> readAndParse(String src) {
        List<Message> ret = new ArrayList<>();
        Scanner sc = new Scanner(src);
        while (sc.hasNext()) {
            String message = sc.next();
            Deque<Character> msg = toStack(message);
            boolean isValid = (this.parse(msg) && msg.size()==0);
            Message m = new Message(message, isValid);
            ret.add(m);
        }
        return ret;
    }

    private Deque<Character> toStack(String message) {
        Character[] ccc = new Character[message.length()];
        for( int i=0; i < message.length(); i++)
            ccc[i] = message.charAt(i);
        List<Character> chars = Arrays.asList(ccc);
        return new ArrayDeque<>(chars);
    }

    static String LOW_CHARS = "abcdefghij";
    static String UP_CHARS = "MKPQ";
    static String Z_CHAR = "Z";

    /**
     * Valid message is composed as
     *
     * LCHAR := ( a | b | c | d | e | f | g | h | i | j )
     * UPCHAR := ( M | K | P | Q )
     * ZCHAR := ( Z )
     * MSG := (LCHAR) | (ZCHAR)(MSG) | (UPCHAR)(MSG)(MSG)
     */
    private boolean parse(Deque<Character> chars) {
        Character ch;
        if( chars.size() > 0 )
            ch = chars.pop();
        else
            return false;

        if ( LOW_CHARS.indexOf(ch) >= 0 )
            return true;
        else if( Z_CHAR.indexOf(ch) >= 0)
            return parseZmsg(chars);
        else if( UP_CHARS.indexOf(ch) >= 0)
            return parseQmsg(chars);
        else
            return false;

    }

    private boolean parseQmsg(Deque<Character> chars) {
        boolean s1 = parse(chars);
        boolean s2 = parse(chars);

        return s1 && s2;
    }

    private boolean parseZmsg(Deque<Character> chars) {
        boolean s1 = parse(chars);
        return s1;
    }

}
