import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class MessageReaderTest {


    @Test
    public void testRead() {
        MessageReader messageReader = new MessageReader();
        List<Message> expected = Arrays.asList(
                new Message[]{
                        new Message("Khfa", false),
                        new Message("MZca", true),
                        new Message("Zj", true),
                        new Message("MKaZcMKaZca", true),
                        new Message("MKaZcaMKaZca", false),
                        new Message("Qa", false),
                        new Message("ZZZZZZj", true),
                        new Message("ZZj", true),
                });
        StringBuilder sb = new StringBuilder();
        for(Message msg: expected) {
            sb.append(msg.msg).append(" ");
        }
        List<Message> actual = messageReader.readAndParse(sb.toString());

        Assert.assertTrue(actual.size() == expected.size());

        for (int i = 0; i < actual.size(); i++) {
            Message act = actual.get(i);
            System.out.printf("actual  : %s\n", act);
            Message exp = expected.get(i);
            System.out.printf("expected: %s\n", exp);
        }
        System.out.println("--------");


        boolean bb = actual.containsAll(expected);
        Assert.assertTrue(bb);

    }
}
