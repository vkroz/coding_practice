package big_home_work.spreadsheet_calc;

public class InvalidDataException extends Exception {
    public InvalidDataException(String s) {
        super(s);
    }
}
