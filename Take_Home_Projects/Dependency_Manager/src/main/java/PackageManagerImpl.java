import java.util.List;
import java.util.Map;

public class PackageManagerImpl implements PackageManager {
    Map<String, List<String>> packages;

    @Override public void add(String item, List<String> dependencies)
            throws IllegalArgumentException {
        for(String dep: dependencies) {
            if(!packages.containsKey(dep)) {
                throw new IllegalArgumentException("required dependency " + dep + " is not found");
            }
        }
        packages.put(item, dependencies);
    }

    @Override public void remove(List<String> items)
            throws IllegalArgumentException {
        // check if all requested packages can be removed
        boolean canRemoveBundle = true;
        for(String item: items) {
            canRemoveBundle = !_canRemove(item) ? false : canRemoveBundle;
        }

        // Do atomic operation - all or nothing
        if(canRemoveBundle) {

        }
    }

    private void _remove(String item)
            throws IllegalArgumentException {

    }

    private boolean _canRemove(String item) {
        boolean canRemove = true;
        for(String packageItem: packages.keySet()) {
            List<String> deps = packages.get(packageItem);
            canRemove = deps.contains(item) ? false : canRemove;
        }
        return canRemove;
    }

    @Override public List<String> list() {
        return null;
    }
}
