import java.util.Scanner;


public class DriverMain {

    public static void main(String[] args) {
        boolean state = true;
        while (state == true) {
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            if(input.startsWith("quit")) {
                System.out.println("bye");
                System.exit(0);
            }
            else if(input.matches("add [a-z]( [a-z])?(,[a-z])*")) {
                System.out.println("installing package");
            }
            else if(input.matches("remove [a-z](,[a-z])*")) {
                System.out.println("removing package");
            }
            else if(input.startsWith("list")) {
                System.out.println("listng packages");
            }
            else {
                System.out.println("unknown command");
            }

        }
    }
}
