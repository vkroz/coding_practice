import java.util.List;
import java.util.Map;

interface PackageManager {

    void add(String item, List<String> dependencies) throws IllegalArgumentException;
    void remove(List<String> items) throws IllegalArgumentException;
    List<String> list();
}
