You need to build simplified version of Linux-style package manager that works via  command line. The purpose is to mimic logic of dependencies management in real package managers. Your program should be capable to add new package, remove package and list installed packages. 

A package is identified by ASCII single character, e.g. `a`, `b`, `c` etc

Packages can have dependencies on each other. For example, package `a` can depend on `b`, while `b` 
can depend on `c` and `f`, and so on. Circular dependencies are prohibited, the program should reject 
input containing circular dependencies.

Your goal is to implement command line version of package manager, that supports 3 commands:

### add
Adds new package. 

Syntax:
```
  add <package id>  [<dependency id1>[, <dependency id2> ...]]
```  
Example:
```
  add a 
  add b a
  add f b,a
```
Notes:

any specified dependencies should be already installed
circular dependencies are not allowed

### remove

Removes one or more installed packages. 
Syntax:
```

  remove <package id1> [, <package id2> [, <package id3> ...]]
```
Example:
```

  remove f 
  remove a, b
```


### list

Prints list of installed packages and their dependencies to console 

 
Testing commands sequence
```
add a           
# doesn't print anything, adds package `a` to the list of installed packages

add b
# adds package `b` to the list of installed packages

add c   a,b
# adds package `c` depending on packages `a` and `b` 
# Since `a` and `b` are already installed, command completes successfully 

list
# lists all installed packages and their dependencies
> a
> b
> c a,b

add x c,d
# adds package `x` depending on packages `c` and `d`
# Since dependency `d` is not installed, command will return error 

add d   b,c
# adds package `d` depending on packages `b` and `c`

add x   c,d
# adds package `x` depending on packages `c` and `d`
# Since dependency `d` is installed, command will complete successfully 

list
> a
> b
> c   a,b
> d   b,c
> x   c,d

remove a
# removes package `a`
# Since there are packages depending on `a`, command will return error 

remove x
# removes package `x`
# Command will complete successfully 

remove d,c,a
# removes packages `d`, `c`, and `a`
# Command will complete successfully because removing these packages won't create unresolved dependencies  

list
> b
```


Once done, submit your solution via email, providing two files

`depman.jar`  - executable jar, that can be runned by command `java -jar depman.jar`
`depman.tar.gz` - project sources that can be unpacked and successfully build
 

 

