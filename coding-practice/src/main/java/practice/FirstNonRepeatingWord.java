package practice;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Find first non-repeating word in 100GB file.
 *
 * <p>We should read all words from the file sequentially and remove all duplicates we find, while
 * keeping ordered list of unique words. Once we know for certain which words are unique (which we
 * can only do after reaching end of file), we'll chose the first in the list
 *
 * <p>1. Naive approach. Assuming that input data is human-readable text, and that typical human
 * language contains max ~ 20K words, then we can fit all consumed words into LInkedHashMap.
 */
public class FirstNonRepeatingWord {
  public Optional<String> solution1(List<String> words) {
    // word -> count
    HashMap<String, Integer> allWords = new HashMap<>();
    // unique words in the order of appearance
    LinkedHashSet<String> uniqueWords = new LinkedHashSet<>();

    words
        .stream()
        .forEach(
            w -> {
              if (allWords.containsKey(w)) {
                allWords.put(w, allWords.get(w) + 1);
                if (uniqueWords.contains(w)) uniqueWords.remove(w);

              } else {
                allWords.put(w, 1);
                uniqueWords.add(w);
              }
            });
    return uniqueWords.iterator().hasNext() ? Optional.of(uniqueWords.iterator().next()) : Optional.empty();
  }

  @Test
  public void testSolution() {
      assertFalse(solution1(Arrays.asList("aa", "bb", "cc", "aa", "bb", "cc")).isPresent());
      assertFalse(solution1(Arrays.asList()).isPresent());
      assertEquals(solution1(Arrays.asList("aa", "bb", "cc", "ac", "ba", "ba", "aa", "cc")), Optional.of("bb"));
  }
}
