package practice;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Find top N elements in input stream
 * Definition of "top" - number of the most frequent values in the stream
 * E.g. in teh sequence
 * `1 2 3 4 1 2 3 1 2 1` top 2 elements will be 1 and 2
 */
public class TopN_Numbers {


    /**
     * Returns top N numbers from stream of integers
     */
    public List<Integer> solution(Iterable<Integer> numbers, int n) {
        TreeSet<Integer> pq = new TreeSet<>(Collections.reverseOrder());

        for (Integer i : numbers) {
            if(pq.size() < n ) {
                pq.add(i);
            } else if (i > pq.last()) {
                pq.remove(pq.last());
                pq.add(i);
            }
        }
        return new ArrayList<>(pq);
    }

    @Test public void testSolution() {
        assertEquals(solution(Arrays.asList(1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 5), 2),
                Arrays.asList(5, 4));
    }
}



