package practice;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Return top N words from endless input stream
 */
public class TopN_WordsCount {


    Map<String, Integer> words = new HashMap<>();

    public void add(String word) {
        if( words.containsKey(word)) {
            words.put( word, words.get(word) + 1);
        }
        else {
            words.put(word, 1);
        }
    }

    public List<String> topWords(int n) {
        List<Map.Entry<String, Integer>> list = new ArrayList<>(words.entrySet());
        list.sort(Map.Entry.comparingByValue(Collections.reverseOrder()));

        List<String> result = new ArrayList<>(n);
        for (int i=0; i < n; i++) {
            result.add(list.get(i).getKey());
        }
        return result;
    }

    Map<K,V> topTen =
            map.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .limit(10)
                    .collect(Collectors.toMap(
                            Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    /**
     * Returns top N numbers from stream of integers
     */
    public List<String> solution(Iterable<String> words, int n) {

        words.forEach(word -> this.add(word));
        return this.topWords(n);
    }

    @Test public void testSolution() {
        List<String> words =
                Arrays.asList("aa", "bb", "cc", "dd", "aa", "bb", "cc", "aa", "bb", "aa");
        assertEquals(Arrays.asList("aa", "bb"), solution(words, 2));
    }



}



