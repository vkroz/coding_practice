

| Source                    | Desc  
|-------------              |----------------------
| FirstNonRepeatingWord     | Find first non-repeating word in the 100GB file
| TopN                      | Find top N numbers in stream of integers
