#!/usr/bin/env bash
# http://stackoverflow.com/questions/5189560/squash-my-last-x-commits-together-using-git
base=HEAD~2
git rebase -i $base


# OR
#   git reset --soft HEAD~3 &&
#   git commit